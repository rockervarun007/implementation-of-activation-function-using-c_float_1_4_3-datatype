TOPFILE=tb_float.bsv
BSVINCDIR=.:%/Libraries
TOP_MODULE=mktb_float

VERILOGDIR=verilog/
BUILDDIR=intermediate/
BS_VERILOG_LIB=/home/giri/gk/bsc/inst/lib/Verilog/
BSVOUTDIR=out/

VERILATOR_FLAGS:= -O3 -LDFLAGS "-static" --x-assign 0  --x-initial unique --noassert sim_main.cpp --bbox-sys -Wno-STMTDLY  -Wno-UNOPTFLAT -Wno-WIDTH -Wno-lint -Wno-COMBDLY -Wno-INITIALDLY  --autoflush   --threads 1 -DBSV_RESET_FIFO_HEAD  -DBSV_RESET_FIFO_ARRAY --output-split 20000  --output-split-ctrace 10000

VERILATOR_SPEED:= OPT_FAST="-O3" OPT_SLOW="-O3" OPT="-O3 "

.PHONY: generate_verilog
generate_verilog:
	@mkdir -p $(VERILOGDIR) $(BUILDDIR)
	@bsc -u -verilog -elab -vdir $(VERILOGDIR) -bdir $(BUILDDIR) -info-dir $(BUILDDIR) +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals -remove-false-rules -remove-empty-rules -remove-starved-rules -remove-dollar -unspecified-to X -show-schedule -show-module-use  -p $(BSVINCDIR) $(TOPFILE)

.PHONY: link_verilator 
link_verilator:
	@echo "Linking $(TOP_MODULE) using verilator"
	@mkdir -p $(BSVOUTDIR) obj_dir
	@echo "#define TOPMODULE V$(TOP_MODULE)" > sim_main.h
	@echo '#include "V$(TOP_MODULE).h"' >> sim_main.h
	verilator $(VERILATOR_FLAGS) --cc $(TOP_MODULE).v -y $(VERILOGDIR) \
		-y $(BS_VERILOG_LIB) -y components/common_verilog --exe --no-timing
	@ln -f -s ./sim_main.cpp obj_dir/sim_main.cpp
	@ln -f -s ./sim_main.h obj_dir/sim_main.h
	make $(VERILATOR_SPEED) VM_PARALLEL_BUILDS=1 -j4 -C obj_dir -f V$(TOP_MODULE).mk
	@cp obj_dir/V$(TOP_MODULE) $(BSVOUTDIR)/out

.PHONY: simulate
simulate:
	

.PHONY: clean
clean:
	@rm -rf $(BUILDDIR) $(BSVOUTDIR) obj_dir $(VERILOGDIR)
