// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float__Syms.h"
#include "Vmktb_float___024root.h"

void Vmktb_float___024root___ctor_var_reset(Vmktb_float___024root* vlSelf);

Vmktb_float___024root::Vmktb_float___024root(Vmktb_float__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    Vmktb_float___024root___ctor_var_reset(this);
}

void Vmktb_float___024root::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

Vmktb_float___024root::~Vmktb_float___024root() {
}
