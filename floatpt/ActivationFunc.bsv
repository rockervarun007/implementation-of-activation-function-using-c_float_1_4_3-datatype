package ActivationFunc;
import Floatingpoint::*;
import ClientServer::*;
import GetPut::*;
import FixedPoint :: * ;



 function  FloatingPoint#(4,3,6) int_lut (Bit#(5) x);
    
      case(x)                                     
              5'b00000 : return unpack(14'b00001000000001); //0
              5'b00001 : return unpack(14'b00001011000000); //1
              5'b00010 : return unpack(14'b00010111000000); //2
              5'b00011 : return unpack(14'b00100010000000); //3 
              5'b00100 : return unpack(14'b00101110000000); //4
              5'b00101 : return unpack(14'b00111001000000); //5
              5'b00110 : return unpack(14'b01000101000000); //6
              5'b00111 : return unpack(14'b01010001000000); //7
              5'b01000 : return unpack(14'b01011100000000); //8
              5'b01001 : return unpack(14'b01101000000000); //9
              5'b01010 : return unpack(14'b01110011000000); //10
              5'b01011 : return unpack(14'b01111111000000); //11
              5'b01100 : return unpack(14'b01111111000000); //12
              5'b01101 : return unpack(14'b01111111000000); //13
              5'b01110 : return unpack(14'b01111111000000); //14
              5'b01111 : return unpack(14'b01111111000000); //15
             
              5'b10000 : return unpack(14'b00001000000001); //-0
              5'b10001 : return unpack(14'b00000011000000); //-1
              5'b10010 : return unpack(14'b00001001000100); //-2
              5'b10011 : return unpack(14'b00001101000110); //-3 
              5'b10100 : return unpack(14'b00010001001000); //-4
              5'b10101 : return unpack(14'b00010110001010); //-5
              5'b10110 : return unpack(14'b00011010001100); //-6
              5'b10111 : return unpack(14'b00011111001110); //-7
              5'b11000 : return unpack(14'b00100011010000); //-8
              5'b11001 : return unpack(14'b00101000010010); //-9
              5'b11010 : return unpack(14'b00101100010100); //-10
              5'b11011 : return unpack(14'b00110001010110); //-11
              5'b11100 : return unpack(14'b00110101011000); //-12
              5'b11101 : return unpack(14'b00111001011010); //-13
              5'b11110 : return unpack(14'b00111110011100); //-14
              5'b11111 : return unpack(14'b01000010011110); //-15
                        
                        
             endcase

 endfunction 

function  FloatingPoint#(4,3,6) fac_lut (Bit#(5) x);
          
          case(x)  
               
              5'b00000 : return unpack(14'b00001000000001); //0.0000
              5'b00001 : return unpack(14'b00001001000001); //0.0625
              5'b00010 : return unpack(14'b00001001000001); //0.1250
              5'b00011 : return unpack(14'b00001010000001); //0.1875 
              5'b00100 : return unpack(14'b00001010000001); //0.2500
              5'b00101 : return unpack(14'b00001011000001); //0.3125
              5'b00110 : return unpack(14'b00001100000001); //0.3750
              5'b00111 : return unpack(14'b00001100000001); //0.4375
              5'b01000 : return unpack(14'b00001101000001); //0.5000
              5'b01001 : return unpack(14'b00001110000001); //0.5625
              5'b01010 : return unpack(14'b00001111000001); //0.6250
              5'b01011 : return unpack(14'b00010000000001); //0.6875
              5'b01100 : return unpack(14'b00010000000001); //0.7500
              5'b01101 : return unpack(14'b00010001000001); //0.8125
              5'b01110 : return unpack(14'b00010010000001); //0.8750
              5'b01111 : return unpack(14'b00010010000001); //0.9375 
                          
              5'b10000 : return unpack(14'b00001000000001); //-0.0000
              5'b10001 : return unpack(14'b00010111000011); //-0.0625
              5'b10010 : return unpack(14'b00011110000100); //-0.1250
              5'b10011 : return unpack(14'b00100101000101); //-0.1875 
              5'b10100 : return unpack(14'b00101100000110); //-0.2500
              5'b10101 : return unpack(14'b00101100000110); //-0.3125
              5'b10110 : return unpack(14'b00101011000110); //-0.3750
              5'b10111 : return unpack(14'b00110010000111); //-0.4375
              5'b11000 : return unpack(14'b00110010000111); //-0.5000
              5'b11001 : return unpack(14'b00111001001000); //-0.5625
              5'b11010 : return unpack(14'b00111001001000); //-0.6250
              5'b11011 : return unpack(14'b01000000001001); //-0.6875
              5'b11100 : return unpack(14'b01000111001010); //-0.7500
              5'b11101 : return unpack(14'b01001110001011); //-0.8125
              5'b11110 : return unpack(14'b01010101001100); //-0.8750
              5'b11111 : return unpack(14'b01010101001100); //-0.9375 
              
           
             endcase 
           

endfunction


 interface Ifc_FloatingPointExp;            
       method ActionValue#(FloatingPoint#(4,3,6)) ma_start(FloatingPoint#(4,3,6)  x, RoundMode rmode, Bit#(3) ac_func ) ; 
     	      
   endinterface 
(*synthesize*)
module mkFloatingPointExp(Ifc_FloatingPointExp); 

   
      Reg#(Bit#(1)) rg_start  <- mkRegA(0);
      Reg#(Bit#(1)) rg_finish  <- mkRegA(0);

     
     function FixedPoint#(17,15)  fconv( Int#(7) exp , Bit#(4) sfd) ; 
        
          FixedPoint#(17,15) fix_val = unpack({ 16'b0,sfd,12'b0});
                 if (exp > 0) begin 
                     return (fix_val << exp);end
                   else   begin       
                       return ( fix_val >> (~exp + 1) );  end    
     endfunction 

                   
     
                  method ActionValue#(FloatingPoint#(4,3,6)) ma_start(FloatingPoint#(4,3,6)  x, RoundMode rmode, Bit#(3) ac_func) ;  
             
                   
                       Int#(7) x_exp = zeroExtend(unpack(x.exp)) - zeroExtend(unpack(x.bias));  
                       Bit#(4) xsfd = { getHiddenBit(x), x.sfd };
                         //$display("%d x.exp \n",x.exp);
                          //$display("%d x.bias \n",x.bias);
                         //$display("%b x_exp \n",x_exp);
                         //$display("%b xsfd \n",xsfd);
            
                           Bit#(32) lv_res = pack(fconv (x_exp, xsfd));
                           FixedPoint#(17,15) lv_res_fix =  unpack(lv_res);
                                $display("%b lv_res_fix \n",lv_res_fix);  
                                 fxptWrite(10,lv_res_fix);  
                                          $display("\n");
                                          
                                   FloatingPoint#(4,3,6) lv_Imprecise_out; 
                                    lv_Imprecise_out.sign = False     ;
	                            lv_Imprecise_out.exp  = 4'b0000   ;
	                            lv_Imprecise_out.sfd  = 3'b000    ;
	                            lv_Imprecise_out.bias = 6'b000000 ; 
	                            
	                           FloatingPoint#(4,3,6) lv_one ;        
	                           lv_one.sign = False     ;
	                           lv_one.exp  = 4'b0010   ;
	                           lv_one.sfd  = 3'b000    ;
	                           lv_one.bias = 6'b000010 ; 
                       
                              if( |lv_res[9:0] == 1 ) begin 
                              
                                 
                                   FloatingPoint#(4,3,6) temp_by_16;        
	                           temp_by_16.sign = x.sign    ;
	                           temp_by_16.exp = x.exp      ;
	                           temp_by_16.sfd = x.sfd      ;
	                           temp_by_16.bias = x.bias + 4;
	                           
	                           //$display("%b temp_by_16 \n",temp_by_16); 
	                              
			           FloatingPoint#(4,3,6) temp_by_4;        
		                   temp_by_4.sign = x.sign    ;
			           temp_by_4.exp = x.exp      ;
			           temp_by_4.sfd = x.sfd      ;
		                   temp_by_4.bias = x.bias + 2;
		                   
		                   //$display("%b temp_by_4 \n",temp_by_4); 
                                      
                                   FloatingPoint#(4,3,6) temp_by_2;        
                                   temp_by_2.sign = x.sign    ;
	                           temp_by_2.exp = x.exp      ;
	                           temp_by_2.sfd = x.sfd      ;
                                   temp_by_2.bias = x.bias + 2; 
                                      
                                   //$display("%b temp_by_2 \n",temp_by_2);    
                                      
                                  
	                           
	                           //$display("%b lv_one \n",lv_one); 
                                    
		                      FloatingPoint#(4,3,6) lv_add_1 = addFP(temp_by_16,temp_by_4 , rmode);  
		                      
		                      //$display("%b lv_add_1 \n",lv_add_1);   
		                           
			              FloatingPoint#(4,3,6) out_1 ; 
			              out_1.sign =  !lv_add_1.sign;
			              out_1.exp  = lv_add_1.exp   ;
			              out_1.sfd  = lv_add_1.sfd   ;
			              out_1.bias = lv_add_1.bias  ;    
			              
			              //$display("%b out_1 \n",out_1);                     
		                     
		                      FloatingPoint#(4,3,6) lv_comp_out_1 = addFP(lv_one,out_1, rmode) ; 
		                      
		                      //$display("%b lv_comp_out_1 \n",lv_comp_out_1); 
		                      
		                      FloatingPoint#(4,3,6) lv_mul_1 = multFP(temp_by_2 , lv_comp_out_1, rmode);
		                      
		                      //$display("%b lv_mul_1 \n",lv_mul_1); 
		                      
		                      FloatingPoint#(4,3,6) out_2 ; 
			              out_2.sign =  !lv_mul_1.sign;
			              out_2.exp  = lv_mul_1.exp   ;
			              out_2.sfd  = lv_mul_1.sfd   ;
			              out_2.bias = lv_mul_1.bias  ; 
			              
			              //$display("%b out_2 \n",out_2);                            
		                     
		                      FloatingPoint#(4,3,6) lv_comp_out_2 = addFP(lv_one,out_2 , rmode); 
		                      
		                      //$display("%b lv_comp_out_2 \n",lv_comp_out_2);  
		                      
		                      FloatingPoint#(4,3,6) lv_mul_2 = multFP( x , lv_comp_out_2 , rmode);
		                      
		                      //$display("%b lv_mul_2 \n",lv_mul_2); 
		                       
		                      FloatingPoint#(4,3,6) out_3 ; 
			              out_3.sign = !lv_mul_2.sign ;
			              out_3.exp  =  lv_mul_2.exp  ;
			              out_3.sfd  =  lv_mul_2.sfd  ;
			              out_3.bias =  lv_mul_2.bias ;  
			              
			              //$display("%b out_3 \n",out_3);    
		                     
		                      lv_Imprecise_out = addFP(lv_one,out_3 , rmode); 
		                      
		                      //$display("%b lv_Imprecise_out \n",lv_Imprecise_out); 
                                          
                                     
                                 end             
                           
                              let precise_int_part = int_lut( { pack(x.sign),lv_res[18:15]}); 
                              let precise_frac_part = fac_lut( { pack(x.sign),lv_res[14:11]});
                            //  let precise_imprecise_frac_part = multFP(precise_frac_part,lv_Imprecise_out , rmode); 
                           
                             FloatingPoint#(4,3,6)  lv_exp = multFP(precise_int_part , precise_frac_part , rmode) ;   
                             //$display("%b lv_Imprecise_out \n",lv_Imprecise_out); 
                             $display("\n %b lv_exp \n",lv_exp); 
                             
                             FloatingPoint#(4,3,6) zero ; 
			                    zero.sign = True ;
			                    zero.exp  =  4'b0010  ;
			                    zero.sfd  =  3'b000 ;
			                    zero.bias =  6'b000010 ;
			                    
                             FloatingPoint#(4,3,6) activ_func = zero; 
                              
                              FloatingPoint#(4,3,6) lv_neg_one ;        
	                           lv_neg_one.sign = True     ;
	                           lv_neg_one.exp  = 4'b0010   ;
	                           lv_neg_one.sfd  = 3'b000    ;
	                           lv_neg_one.bias = 6'b000010 ;
                             
                                                         
                              case(ac_func)  
				       3'b000 : if (x.sign == True )  begin//relu				                     
				                     activ_func = zero; end 
				                     else begin 
				                     activ_func = lv_exp; end  
				        
				       3'b001 : if (x.sign == True )  begin                    //leaky relu
				       
				                    FloatingPoint#(4,3,6) lv_leaky_relu ; 
			                              lv_leaky_relu.sign =  True ;
			                              lv_leaky_relu.exp  =  x.exp  ;
			                              lv_leaky_relu.sfd  =  x.sfd ;
			                              lv_leaky_relu.bias =  x.bias + 8 ;
			                                
				                     activ_func = lv_leaky_relu ; end 
				                     else begin 
				                     activ_func = x ; end 
				                     
				       3'b010 : begin
				                   FloatingPoint#(4,3,6) lv_exp_plus_one =  addFP(lv_exp,lv_one,rmode);
				                   //$display("%b lv_exp_plus_one \n",lv_exp_plus_one); 
				                    activ_func = divFP(lv_exp, lv_exp_plus_one, rmode); end      // sigmoid
				       
				       3'b011 :if (x.sign == True )  begin                    //Selu
				                     activ_func = x  ; end 
				                     else begin 
				                     FloatingPoint#(4,3,6) lv_exp_minus_one =  addFP(lv_exp,lv_neg_one,rmode); 
				                     activ_func = lv_exp_minus_one; end 
				       
				       default : begin 
				                  let lv_exp2 = multFP(lv_exp,lv_exp,rmode) ;  
				                 FloatingPoint#(4,3,6) lv_exp_minus_one =  addFP(lv_exp2,lv_neg_one,rmode);
				                 FloatingPoint#(4,3,6) lv_exp_plus_one =  addFP(lv_exp2,lv_one,rmode);
				                  activ_func = divFP(lv_exp_minus_one,lv_exp_plus_one, rmode) ; end  //tanh
				        
                                                                                                      
                                    endcase
           
                                     $display("\n %b activ_func \n",activ_func); 
                           

                           
                          return(lv_exp); 
                         
                                                      
                   endmethod
           
endmodule 



endpackage
