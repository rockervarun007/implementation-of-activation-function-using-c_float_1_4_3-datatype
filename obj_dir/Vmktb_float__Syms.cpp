// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "Vmktb_float__Syms.h"
#include "Vmktb_float.h"
#include "Vmktb_float___024root.h"

// FUNCTIONS
Vmktb_float__Syms::~Vmktb_float__Syms()
{
}

Vmktb_float__Syms::Vmktb_float__Syms(VerilatedContext* contextp, const char* namep, Vmktb_float* modelp)
    : VerilatedSyms{contextp}
    // Setup internal state of the Syms class
    , __Vm_modelp{modelp}
    // Setup module instances
    , TOP{this, namep}
{
    // Configure time unit / time precision
    _vm_contextp__->timeunit(-12);
    _vm_contextp__->timeprecision(-12);
    // Setup each module's pointers to their submodules
    // Setup each module's pointer back to symbol table (for public functions)
    TOP.__Vconfigure(true);
    // Setup scopes
    __Vscope_mktb_float__exp__fpadder_1_fOperands_S0.configure(this, name(), "mktb_float.exp.fpadder_1_fOperands_S0", "fpadder_1_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpadder_1_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S1.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S1", "fpadder_1_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S2.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S2", "fpadder_1_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S3.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S3", "fpadder_1_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S4.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S4", "fpadder_1_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_1_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpadder_1_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fOperands_S0.configure(this, name(), "mktb_float.exp.fpadder_2_fOperands_S0", "fpadder_2_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpadder_2_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S1.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S1", "fpadder_2_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S2.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S2", "fpadder_2_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S3.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S3", "fpadder_2_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S4.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S4", "fpadder_2_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_2_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpadder_2_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fOperands_S0.configure(this, name(), "mktb_float.exp.fpadder_3_fOperands_S0", "fpadder_3_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpadder_3_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S1.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S1", "fpadder_3_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S2.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S2", "fpadder_3_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S3.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S3", "fpadder_3_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S4.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S4", "fpadder_3_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_3_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpadder_3_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fOperands_S0.configure(this, name(), "mktb_float.exp.fpadder_4_fOperands_S0", "fpadder_4_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpadder_4_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S1.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S1", "fpadder_4_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S2.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S2", "fpadder_4_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S3.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S3", "fpadder_4_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S4.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S4", "fpadder_4_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpadder_4_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpadder_4_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fOperands_S0.configure(this, name(), "mktb_float.exp.fpmul_1_fOperands_S0", "fpmul_1_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpmul_1_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S1.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S1", "fpmul_1_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S2.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S2", "fpmul_1_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S3.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S3", "fpmul_1_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S4.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S4", "fpmul_1_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_1_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpmul_1_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fOperands_S0.configure(this, name(), "mktb_float.exp.fpmul_2_fOperands_S0", "fpmul_2_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpmul_2_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S1.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S1", "fpmul_2_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S2.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S2", "fpmul_2_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S3.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S3", "fpmul_2_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S4.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S4", "fpmul_2_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_2_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpmul_2_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fOperands_S0.configure(this, name(), "mktb_float.exp.fpmul_3_fOperands_S0", "fpmul_3_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpmul_3_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S1.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S1", "fpmul_3_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S2.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S2", "fpmul_3_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S3.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S3", "fpmul_3_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S4.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S4", "fpmul_3_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_3_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpmul_3_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fOperands_S0.configure(this, name(), "mktb_float.exp.fpmul_4_fOperands_S0", "fpmul_4_fOperands_S0", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fOperands_S0__error_checks.configure(this, name(), "mktb_float.exp.fpmul_4_fOperands_S0.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S1.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S1", "fpmul_4_fState_S1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S1__error_checks.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S1.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S2.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S2", "fpmul_4_fState_S2", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S2__error_checks.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S2.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S3.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S3", "fpmul_4_fState_S3", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S3__error_checks.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S3.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S4.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S4", "fpmul_4_fState_S4", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_mktb_float__exp__fpmul_4_fState_S4__error_checks.configure(this, name(), "mktb_float.exp.fpmul_4_fState_S4.error_checks", "error_checks", -12, VerilatedScope::SCOPE_OTHER);
}
