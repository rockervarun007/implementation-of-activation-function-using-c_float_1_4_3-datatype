// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Model implementation (design independent parts)

#include "Vmktb_float.h"
#include "Vmktb_float__Syms.h"

//============================================================
// Constructors

Vmktb_float::Vmktb_float(VerilatedContext* _vcontextp__, const char* _vcname__)
    : VerilatedModel{*_vcontextp__}
    , vlSymsp{new Vmktb_float__Syms(contextp(), _vcname__, this)}
    , CLK{vlSymsp->TOP.CLK}
    , RST_N{vlSymsp->TOP.RST_N}
    , rootp{&(vlSymsp->TOP)}
{
    // Register model with the context
    contextp()->addModel(this);
}

Vmktb_float::Vmktb_float(const char* _vcname__)
    : Vmktb_float(Verilated::threadContextp(), _vcname__)
{
}

//============================================================
// Destructor

Vmktb_float::~Vmktb_float() {
    delete vlSymsp;
}

//============================================================
// Evaluation function

#ifdef VL_DEBUG
void Vmktb_float___024root___eval_debug_assertions(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG
void Vmktb_float___024root___eval_static(Vmktb_float___024root* vlSelf);
void Vmktb_float___024root___eval_initial(Vmktb_float___024root* vlSelf);
void Vmktb_float___024root___eval_settle(Vmktb_float___024root* vlSelf);
void Vmktb_float___024root___eval(Vmktb_float___024root* vlSelf);

void Vmktb_float::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vmktb_float::eval_step\n"); );
#ifdef VL_DEBUG
    // Debug assertions
    Vmktb_float___024root___eval_debug_assertions(&(vlSymsp->TOP));
#endif  // VL_DEBUG
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) {
        vlSymsp->__Vm_didInit = true;
        VL_DEBUG_IF(VL_DBG_MSGF("+ Initial\n"););
        Vmktb_float___024root___eval_static(&(vlSymsp->TOP));
        Vmktb_float___024root___eval_initial(&(vlSymsp->TOP));
        Vmktb_float___024root___eval_settle(&(vlSymsp->TOP));
    }
    // MTask 0 start
    VL_DEBUG_IF(VL_DBG_MSGF("MTask0 starting\n"););
    Verilated::mtaskId(0);
    VL_DEBUG_IF(VL_DBG_MSGF("+ Eval\n"););
    Vmktb_float___024root___eval(&(vlSymsp->TOP));
    // Evaluate cleanup
    Verilated::endOfThreadMTask(vlSymsp->__Vm_evalMsgQp);
    Verilated::endOfEval(vlSymsp->__Vm_evalMsgQp);
}

//============================================================
// Events and timing
bool Vmktb_float::eventsPending() { return false; }

uint64_t Vmktb_float::nextTimeSlot() {
    VL_FATAL_MT(__FILE__, __LINE__, "", "%Error: No delays in the design");
    return 0;
}

//============================================================
// Utilities

const char* Vmktb_float::name() const {
    return vlSymsp->name();
}

//============================================================
// Invoke final blocks

void Vmktb_float___024root___eval_final(Vmktb_float___024root* vlSelf);

VL_ATTR_COLD void Vmktb_float::final() {
    Vmktb_float___024root___eval_final(&(vlSymsp->TOP));
}

//============================================================
// Implementations of abstract methods from VerilatedModel

const char* Vmktb_float::hierName() const { return vlSymsp->name(); }
const char* Vmktb_float::modelName() const { return "Vmktb_float"; }
unsigned Vmktb_float::threads() const { return 1; }
