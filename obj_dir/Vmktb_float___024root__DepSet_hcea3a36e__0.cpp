// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float___024root.h"

void Vmktb_float___024root___eval_act(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_act\n"); );
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__1(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__1\n"); );
    // Body
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN)) {
            VL_WRITEF("%b: fpadder_4_out\n",14,vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_OUT);
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("%4#: Sent input ",10,vlSelf->mktb_float__DOT__counter);
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("<Float + 0. 0.0>");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("\n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("00000000000000: Sent input \n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__exp_EN_mav_out)) {
            VL_WRITEF("%4#: exponent response: ",10,
                      vlSelf->mktb_float__DOT__counter);
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (vlSelf->mktb_float__DOT__exp_EN_mav_out) {
            if ((0x2000U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out))) {
                VL_WRITEF("<Float -%2#.%2#.%1#>",4,
                          (0xfU & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out) 
                                   >> 9U)),6,(0x3fU 
                                              & (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out)),
                          3,(7U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out) 
                                   >> 6U)));
                Verilated::runFlushCallbacks();
            } else {
                VL_WRITEF("<Float +%2#.%2#.%1#>",4,
                          (0xfU & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out) 
                                   >> 9U)),6,(0x3fU 
                                              & (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out)),
                          3,(7U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_out) 
                                   >> 6U)));
                Verilated::runFlushCallbacks();
            }
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__exp_EN_mav_out)) {
            VL_WRITEF("\n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__exp_EN_mav_out)) {
            VL_WRITEF("%b: exponent response:  \n",
                      14,vlSelf->mktb_float__DOT__exp__DOT__rg_out);
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY((2U == (IData)(vlSelf->mktb_float__DOT__rg_state)))) {
            VL_FINISH_MT("verilog//mktb_float.v", 198, "");
        }
    }
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__2(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__2\n"); );
    // Body
    if (vlSelf->RST_N) {
        if (vlSelf->mktb_float__DOT__rg_state_EN) {
            vlSelf->mktb_float__DOT__exp__DOT__rg_start 
                = (1U & (~ (IData)(vlSelf->mktb_float__DOT__exp_EN_mav_out)));
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN) {
            vlSelf->mktb_float__DOT__exp__DOT__rg_finish = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__rg_out 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_OUT;
        }
        if (vlSelf->mktb_float__DOT__done_EN) {
            vlSelf->mktb_float__DOT__exp__DOT__rg_rmode = 0U;
            vlSelf->mktb_float__DOT__exp__DOT__rg_x = 0U;
        }
    } else {
        vlSelf->mktb_float__DOT__exp__DOT__rg_start = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__rg_finish = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__rg_rmode = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__rg_x = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__rg_out = 0U;
    }
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__3(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__3\n"); );
    // Body
    if (vlSelf->RST_N) {
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_IN;
        }
    } else {
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_OUT = 0U;
    }
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_IN 
        = (((IData)((0x800ULL == (0x200000000800ULL 
                                  & vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                       >> 0x2dU))) ? 0U
                         : ((((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hb011b47d__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                  ? 
                                                 ((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                               >> 6U))))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(
                                                             (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                              >> 6U))))
                                                  : 
                                                 ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_he325a082__0)
                                                   ? 
                                                  ((0U 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                                >> 6U))))
                                                    ? 1U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                               >> 6U)))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_4_fState_S3_first___05F83_B_ETC___05F_d738)) 
                                                       | (IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h97c68da2__0)))
                                                    ? 0U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                               >> 6U))))))) 
                             << 9U) | ((((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hb011b47d__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                       ? 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_he325a082__0)
                                                        ? 
                                                       ((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h97c68da2__0)
                                                         ? (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845)
                                                         : 
                                                        (((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_4_fState_S3_first___05F83_B_ETC___05F_d738)
                                                           ? 
                                                          ((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
                                                           << (IData)(vlSelf->mktb_float__DOT__exp__DOT__IF_IF_fpadder_4_fState_S3_first___05F83_BIT_10_88___05FETC___05F_d735))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))) 
                                                                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_ha31bf39d__0))))
                                                      ? (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT)
                                                      : 
                                                     ((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_4_fState_S3_first___05F83_B_ETC___05F_d738)
                                                       ? 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT) 
                                                       + (IData)(vlSelf->mktb_float__DOT__exp__DOT__IF_IF_fpadder_4_fState_S3_first___05F83_BIT_10_88___05FETC___05F_d735))
                                                       : 0x3fU)))))));
}

VL_INLINE_OPT void Vmktb_float___024root___nba_comb__TOP__0(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_comb__TOP__0\n"); );
    // Body
    vlSelf->mktb_float__DOT__done_EN = ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_start)) 
                                        & (0U == (IData)(vlSelf->mktb_float__DOT__rg_state)));
    vlSelf->mktb_float__DOT__exp_EN_mav_out = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_finish) 
                                               & (1U 
                                                  == (IData)(vlSelf->mktb_float__DOT__rg_state)));
    vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN 
        = ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_finish)) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg));
    vlSelf->mktb_float__DOT__rg_state_EN = ((IData)(vlSelf->mktb_float__DOT__done_EN) 
                                            | (IData)(vlSelf->mktb_float__DOT__exp_EN_mav_out));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_FULL_N) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_FULL_N) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_ENQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_FULL_N) 
           & (0x84U > (0xffU & (0x80U ^ ((0xfU & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x) 
                                                  >> 9U)) 
                                         - (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x)))))));
}

void Vmktb_float___024root___nba_sequent__TOP__0(Vmktb_float___024root* vlSelf);

void Vmktb_float___024root___eval_nba(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_nba\n"); );
    // Body
    if (vlSelf->__VnbaTriggered.at(0U)) {
        Vmktb_float___024root___nba_sequent__TOP__0(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(1U)) {
        Vmktb_float___024root___nba_sequent__TOP__1(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(2U)) {
        Vmktb_float___024root___nba_sequent__TOP__2(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(0U)) {
        Vmktb_float___024root___nba_sequent__TOP__3(vlSelf);
    }
    if ((vlSelf->__VnbaTriggered.at(0U) | vlSelf->__VnbaTriggered.at(2U))) {
        Vmktb_float___024root___nba_comb__TOP__0(vlSelf);
    }
}

void Vmktb_float___024root___eval_triggers__act(Vmktb_float___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__act(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG
#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__nba(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG

void Vmktb_float___024root___eval(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval\n"); );
    // Init
    VlTriggerVec<3> __VpreTriggered;
    IData/*31:0*/ __VnbaIterCount;
    CData/*0:0*/ __VnbaContinue;
    // Body
    __VnbaIterCount = 0U;
    __VnbaContinue = 1U;
    while (__VnbaContinue) {
        __VnbaContinue = 0U;
        vlSelf->__VnbaTriggered.clear();
        vlSelf->__VactIterCount = 0U;
        vlSelf->__VactContinue = 1U;
        while (vlSelf->__VactContinue) {
            vlSelf->__VactContinue = 0U;
            Vmktb_float___024root___eval_triggers__act(vlSelf);
            if (vlSelf->__VactTriggered.any()) {
                vlSelf->__VactContinue = 1U;
                if ((0x64U < vlSelf->__VactIterCount)) {
#ifdef VL_DEBUG
                    Vmktb_float___024root___dump_triggers__act(vlSelf);
#endif
                    VL_FATAL_MT("verilog//mktb_float.v", 29, "", "Active region did not converge.");
                }
                vlSelf->__VactIterCount = ((IData)(1U) 
                                           + vlSelf->__VactIterCount);
                __VpreTriggered.andNot(vlSelf->__VactTriggered, vlSelf->__VnbaTriggered);
                vlSelf->__VnbaTriggered.set(vlSelf->__VactTriggered);
                Vmktb_float___024root___eval_act(vlSelf);
            }
        }
        if (vlSelf->__VnbaTriggered.any()) {
            __VnbaContinue = 1U;
            if ((0x64U < __VnbaIterCount)) {
#ifdef VL_DEBUG
                Vmktb_float___024root___dump_triggers__nba(vlSelf);
#endif
                VL_FATAL_MT("verilog//mktb_float.v", 29, "", "NBA region did not converge.");
            }
            __VnbaIterCount = ((IData)(1U) + __VnbaIterCount);
            Vmktb_float___024root___eval_nba(vlSelf);
        }
    }
}

#ifdef VL_DEBUG
void Vmktb_float___024root___eval_debug_assertions(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->CLK & 0xfeU))) {
        Verilated::overWidthError("CLK");}
    if (VL_UNLIKELY((vlSelf->RST_N & 0xfeU))) {
        Verilated::overWidthError("RST_N");}
}
#endif  // VL_DEBUG
