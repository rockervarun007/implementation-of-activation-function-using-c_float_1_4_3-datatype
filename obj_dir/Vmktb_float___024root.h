// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vmktb_float.h for the primary calling header

#ifndef VERILATED_VMKTB_FLOAT___024ROOT_H_
#define VERILATED_VMKTB_FLOAT___024ROOT_H_  // guard

#include "verilated.h"

class Vmktb_float__Syms;

class Vmktb_float___024root final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        VL_IN8(CLK,0,0);
        VL_IN8(RST_N,0,0);
        CData/*0:0*/ mktb_float__DOT__done_EN;
        CData/*1:0*/ mktb_float__DOT__rg_state;
        CData/*0:0*/ mktb_float__DOT__rg_state_EN;
        CData/*0:0*/ mktb_float__DOT__exp_EN_mav_out;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__rg_finish;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__rg_finish_EN;
        CData/*2:0*/ mktb_float__DOT__exp__DOT__rg_rmode;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__rg_start;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_ENQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ;
    };
    struct {
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_FULL_N;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_FULL_N;
        CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993;
        CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193;
        CData/*7:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_first___05F2_BITS_25_TO_18_9_S_ETC___05F_d72;
        CData/*7:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_first___05F54_BITS_25_TO_18_61_ETC___05F_d264;
        CData/*7:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_first___05F46_BITS_25_TO_18_53_ETC___05F_d456;
        CData/*7:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_first___05F38_BITS_25_TO_18_45_ETC___05F_d648;
        CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdin___05Fh10845;
        CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697;
        CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_fpadder_4_fState_S3_first___05F83_BIT_10_88___05FETC___05F_d735;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_fpmul_2_fOperands_S0_first___05F73_BIT_30_009___05FETC___05F_d1012;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_fpmul_3_fOperands_S0_first___05F173_BIT_30_209_ETC___05F_d1212;
        CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_4_fState_S3_first___05F83_B_ETC___05F_d738;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_hb011b47d__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_he325a082__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h97c68da2__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_ha31bf39d__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_haea742db__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h42c3b394__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h9b071e30__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_hf418fd2e__0;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg;
    };
    struct {
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_1_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg;
        CData/*0:0*/ mktb_float__DOT__exp__DOT__fpmul_4_fState_S4__DOT__empty_reg;
        CData/*0:0*/ __Vtrigrprev__TOP__CLK;
        CData/*0:0*/ __Vtrigrprev__TOP__RST_N;
        CData/*0:0*/ __VactContinue;
        SData/*9:0*/ mktb_float__DOT__counter;
        SData/*9:0*/ mktb_float__DOT__counter_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__rg_out;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__rg_x;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_OUT;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_OUT;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_OUT;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_D_OUT;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_OUT;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_IN;
        SData/*13:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT;
        IData/*30:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT;
        IData/*31:0*/ __VstlIterCount;
        IData/*31:0*/ __VactIterCount;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_IN;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_IN;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_IN;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_IN;
        QData/*51:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT;
        QData/*45:0*/ mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT;
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT;
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_D_OUT;
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT;
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT;
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_D_OUT;
    };
    struct {
        QData/*34:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT;
    };
    VlTriggerVec<1> __VstlTriggered;
    VlTriggerVec<3> __VactTriggered;
    VlTriggerVec<3> __VnbaTriggered;

    // INTERNAL VARIABLES
    Vmktb_float__Syms* const vlSymsp;

    // CONSTRUCTORS
    Vmktb_float___024root(Vmktb_float__Syms* symsp, const char* name);
    ~Vmktb_float___024root();
    VL_UNCOPYABLE(Vmktb_float___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
