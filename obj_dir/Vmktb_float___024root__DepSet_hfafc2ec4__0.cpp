// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float__Syms.h"
#include "Vmktb_float___024root.h"

#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__act(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG

void Vmktb_float___024root___eval_triggers__act(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_triggers__act\n"); );
    // Body
    vlSelf->__VactTriggered.at(0U) = ((IData)(vlSelf->CLK) 
                                      & (~ (IData)(vlSelf->__Vtrigrprev__TOP__CLK)));
    vlSelf->__VactTriggered.at(1U) = ((~ (IData)(vlSelf->CLK)) 
                                      & (IData)(vlSelf->__Vtrigrprev__TOP__CLK));
    vlSelf->__VactTriggered.at(2U) = (((IData)(vlSelf->CLK) 
                                       & (~ (IData)(vlSelf->__Vtrigrprev__TOP__CLK))) 
                                      | ((~ (IData)(vlSelf->RST_N)) 
                                         & (IData)(vlSelf->__Vtrigrprev__TOP__RST_N)));
    vlSelf->__Vtrigrprev__TOP__CLK = vlSelf->CLK;
    vlSelf->__Vtrigrprev__TOP__RST_N = vlSelf->RST_N;
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vmktb_float___024root___dump_triggers__act(vlSelf);
    }
#endif
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__0(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__0\n"); );
    // Init
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587;
    CData/*7:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_first___05F051_BITS_16_TO_9_062___05FETC___05F_d1075;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_first___05F251_BITS_16_TO_9_262___05FETC___05F_d1275;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdin___05Fh1658;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdin___05Fh4721;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdin___05Fh7783;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdres___05Fh16868;
    CData/*7:0*/ mktb_float__DOT__exp__DOT__sfdres___05Fh20054;
    CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096;
    CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296;
    CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121;
    CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313;
    CData/*6:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505;
    CData/*5:0*/ mktb_float__DOT__exp__DOT__din_bias___05Fh18325;
    CData/*5:0*/ mktb_float__DOT__exp__DOT__din_bias___05Fh21511;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_2_fState_S3_first___05F051_B_ETC___05F_d1134;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_3_fState_S3_first___05F251_B_ETC___05F_d1334;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_fpadder_1_fState_S3_first___05F07_BIT_10_12___05FETC___05F_d159;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_fpadder_2_fState_S3_first___05F99_BIT_10_04___05FETC___05F_d351;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_IF_fpadder_3_fState_S3_first___05F91_BIT_10_96___05FETC___05F_d543;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_fpmul_2_fState_S3_first___05F051_BITS_8_TO_1_05_ETC___05F_d1086;
    CData/*3:0*/ mktb_float__DOT__exp__DOT__IF_fpmul_3_fState_S3_first___05F251_BITS_8_TO_1_25_ETC___05F_d1286;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609;
    CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_2_fState_S3_fir_ETC___05F_d1137;
    CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_3_fState_S3_fir_ETC___05F_d1337;
    CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_1_fState_S3_first___05F07_B_ETC___05F_d162;
    CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_2_fState_S3_first___05F99_B_ETC___05F_d354;
    CData/*0:0*/ mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_3_fState_S3_first___05F91_B_ETC___05F_d546;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__sfdlsb___05Fh17091;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__sfdlsb___05Fh20277;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h3ff71dbe__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h313e100d__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h02ddd2cc__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h245915ac__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h93f67e5d__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_he89ae0f2__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h8cfd2360__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h840cb8d5__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h1a440f5b__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_hddbfceca__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h78cf662a__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h3dbc9c35__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_hdf31fb6e__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h008074bd__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h5a4b53af__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h67405d8e__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h65459769__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h7036bbee__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_h3324af5c__0;
    CData/*0:0*/ mktb_float__DOT__exp__DOT____VdfgTmp_he3763359__0;
    // Body
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_ENQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S4__DOT__empty_reg) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S4__DOT__empty_reg) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_1_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_4_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_1_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_2_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_2_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_3_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpmul_3_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fOperands_S0.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fOperands_S0.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S1.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S1.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S2.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S2.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S3.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S3.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY(((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg)) 
                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN)))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S4.error_checks -- Dequeuing from empty fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (VL_UNLIKELY((((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4_FULL_N)) 
                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ)) 
                     & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN))))) {
        VL_WRITEF("Warning: FIFOL1: %Nmktb_float.exp.fpadder_4_fState_S4.error_checks -- Enqueuing to a full fifo\n",
                  vlSymsp->name());
        Verilated::runFlushCallbacks();
    }
    if (vlSelf->RST_N) {
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_ENQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                = ((0x7f800000U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x) 
                                   << 0x11U)) | ((0x7e0000U 
                                                  & (((IData)(4U) 
                                                      + (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x)) 
                                                     << 0x11U)) 
                                                 | ((0x1fe00U 
                                                     & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x) 
                                                        << 3U)) 
                                                    | ((0x1f8U 
                                                        & (((IData)(2U) 
                                                            + (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x)) 
                                                           << 3U)) 
                                                       | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode)))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                = (0x8040000U | ((0x10000U & ((~ ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_OUT) 
                                                  >> 0xdU)) 
                                              << 0x10U)) 
                                 | ((0xfff8U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_OUT) 
                                                << 3U)) 
                                    | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                = (0x8040000U | ((0x10000U & ((~ ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_OUT) 
                                                  >> 0xdU)) 
                                              << 0x10U)) 
                                 | ((0xfff8U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_OUT) 
                                                << 3U)) 
                                    | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                                                         >> 0x1cU))))) 
                    << 0x1cU) | (QData)((IData)(((0xff00000U 
                                                  & (((IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                                                               >> 0x14U)) 
                                                      + (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                                                                 >> 0xcU))) 
                                                     << 0x14U)) 
                                                 | ((0xff000U 
                                                     & (((IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                                                                  >> 0x14U)) 
                                                         - (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                                                                    >> 0xcU))) 
                                                        << 0xcU)) 
                                                    | (0xfffU 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT)))))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                                                         >> 0x1cU))))) 
                    << 0x1cU) | (QData)((IData)(((0xff00000U 
                                                  & (((IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                                                               >> 0x14U)) 
                                                      + (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                                                                 >> 0xcU))) 
                                                     << 0x14U)) 
                                                 | ((0xff000U 
                                                     & (((IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                                                                  >> 0x14U)) 
                                                         - (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                                                                    >> 0xcU))) 
                                                        << 0xcU)) 
                                                    | (0xfffU 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT)))))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                = (0x8040000U | ((0x10000U & ((~ ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_OUT) 
                                                  >> 0xdU)) 
                                              << 0x10U)) 
                                 | ((0xfff8U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_OUT) 
                                                << 3U)) 
                                    | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                                                         >> 0x1cU))))) 
                    << 0x1cU) | (QData)((IData)(((0xff00000U 
                                                  & (((IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                                                               >> 0x14U)) 
                                                      + (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                                                                 >> 0xcU))) 
                                                     << 0x14U)) 
                                                 | ((0xff000U 
                                                     & (((IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                                                                  >> 0x14U)) 
                                                         - (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                                                                    >> 0xcU))) 
                                                        << 0xcU)) 
                                                    | (0xfffU 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT)))))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                                                         >> 0x1cU))))) 
                    << 0x1cU) | (QData)((IData)(((0xff00000U 
                                                  & (((IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                                                               >> 0x14U)) 
                                                      + (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                                                                 >> 0xcU))) 
                                                     << 0x14U)) 
                                                 | ((0xff000U 
                                                     & (((IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                                                                  >> 0x14U)) 
                                                         - (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                                                                    >> 0xcU))) 
                                                        << 0xcU)) 
                                                    | (0xfffU 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT)))))));
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_D_OUT;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_D_OUT;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg = 0U;
        }
        vlSelf->mktb_float__DOT__counter = vlSelf->mktb_float__DOT__counter_D_IN;
        if (vlSelf->mktb_float__DOT__rg_state_EN) {
            vlSelf->mktb_float__DOT__rg_state = ((IData)(vlSelf->mktb_float__DOT__done_EN)
                                                  ? 1U
                                                  : 2U);
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0__DOT__empty_reg = 0U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0__DOT__empty_reg = 0U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S4__DOT__empty_reg = 1U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S4__DOT__empty_reg = 1U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffffU & (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                           >> 0x1aU))))) 
                    << 0x14U) | (QData)((IData)(((0xff000U 
                                                  & (((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                                  >> 0x33U)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                                  >> 0x12U))
                                                       : 
                                                      ((8U 
                                                        > 
                                                        (0x3fU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT)))
                                                        ? 
                                                       ((0xfeU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_first___05F2_BITS_25_TO_18_9_S_ETC___05F_d72)) 
                                                        | (1U 
                                                           & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_first___05F2_BITS_25_TO_18_9_S_ETC___05F_d72) 
                                                              | (0U 
                                                                 != 
                                                                 ((7U 
                                                                   >= 
                                                                   (0x3fU 
                                                                    & ((IData)(8U) 
                                                                       - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT))))
                                                                   ? 
                                                                  (0xffU 
                                                                   & ((IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                                               >> 0x12U)) 
                                                                      << 
                                                                      (0x3fU 
                                                                       & ((IData)(8U) 
                                                                          - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT)))))
                                                                   : 0U)))))
                                                        : 
                                                       ((0U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                                     >> 0x12U))))
                                                         ? (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                                    >> 0x12U))
                                                         : 1U))) 
                                                     << 0xcU)) 
                                                 | (0xfffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                                               >> 6U)))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffffU & (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                           >> 0x1aU))))) 
                    << 0x14U) | (QData)((IData)(((0xff000U 
                                                  & (((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                                  >> 0x33U)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                                  >> 0x12U))
                                                       : 
                                                      ((8U 
                                                        > 
                                                        (0x3fU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT)))
                                                        ? 
                                                       ((0xfeU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_first___05F54_BITS_25_TO_18_61_ETC___05F_d264)) 
                                                        | (1U 
                                                           & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_first___05F54_BITS_25_TO_18_61_ETC___05F_d264) 
                                                              | (0U 
                                                                 != 
                                                                 ((7U 
                                                                   >= 
                                                                   (0x3fU 
                                                                    & ((IData)(8U) 
                                                                       - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT))))
                                                                   ? 
                                                                  (0xffU 
                                                                   & ((IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                                               >> 0x12U)) 
                                                                      << 
                                                                      (0x3fU 
                                                                       & ((IData)(8U) 
                                                                          - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT)))))
                                                                   : 0U)))))
                                                        : 
                                                       ((0U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                                     >> 0x12U))))
                                                         ? (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                                    >> 0x12U))
                                                         : 1U))) 
                                                     << 0xcU)) 
                                                 | (0xfffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                                               >> 6U)))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT 
                                                         >> 0x11U))))) 
                    << 0x11U) | (QData)((IData)(((0x1fe00U 
                                                  & (((0xfU 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT 
                                                                  >> 0xdU))) 
                                                      * 
                                                      (0xfU 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT 
                                                                  >> 9U)))) 
                                                     << 9U)) 
                                                 | (0x1ffU 
                                                    & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT 
                = (((QData)((IData)(((0x8fU < (0x80U 
                                               ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993))) 
                                     | ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_haea742db__0) 
                                        | (0x3dU > 
                                           (0x80U ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993))))))) 
                    << 0x22U) | (((QData)((IData)(vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_2_fOperands_S0_first___05F73_BIT_30_009___05FETC___05F_d1012)) 
                                  << 0x21U) | (((QData)((IData)(
                                                                ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h42c3b394__0)
                                                                  ? 0U
                                                                  : 0xfU))) 
                                                << 0x1dU) 
                                               | (QData)((IData)(
                                                                 ((((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h42c3b394__0)
                                                                     ? 0U
                                                                     : 7U) 
                                                                   << 0x1aU) 
                                                                  | ((0xe0000U 
                                                                      & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                                                         << 0x11U)) 
                                                                     | (((0U 
                                                                          != 
                                                                          (0xfU 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                                                              >> 0x1aU))) 
                                                                         << 0x10U) 
                                                                        | ((0xe000U 
                                                                            & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                                                               >> 0xaU)) 
                                                                           | (((0U 
                                                                                != 
                                                                                (0xfU 
                                                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                                                                >> 0xcU))) 
                                                                               << 0xcU) 
                                                                              | ((0xe00U 
                                                                                & vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT) 
                                                                                | (((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993) 
                                                                                << 1U) 
                                                                                | (IData)(vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_2_fOperands_S0_first___05F73_BIT_30_009___05FETC___05F_d1012)))))))))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                = (((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_OUT) 
                    << 0x11U) | ((0x1fe00U & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x) 
                                              << 3U)) 
                                 | ((0x1f8U & (((IData)(1U) 
                                                + (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x)) 
                                               << 3U)) 
                                    | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_ENQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffffU & (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                           >> 0x1aU))))) 
                    << 0x14U) | (QData)((IData)(((0xff000U 
                                                  & (((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                                  >> 0x33U)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                                  >> 0x12U))
                                                       : 
                                                      ((8U 
                                                        > 
                                                        (0x3fU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT)))
                                                        ? 
                                                       ((0xfeU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_first___05F46_BITS_25_TO_18_53_ETC___05F_d456)) 
                                                        | (1U 
                                                           & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_first___05F46_BITS_25_TO_18_53_ETC___05F_d456) 
                                                              | (0U 
                                                                 != 
                                                                 ((7U 
                                                                   >= 
                                                                   (0x3fU 
                                                                    & ((IData)(8U) 
                                                                       - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT))))
                                                                   ? 
                                                                  (0xffU 
                                                                   & ((IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                                               >> 0x12U)) 
                                                                      << 
                                                                      (0x3fU 
                                                                       & ((IData)(8U) 
                                                                          - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT)))))
                                                                   : 0U)))))
                                                        : 
                                                       ((0U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                                     >> 0x12U))))
                                                         ? (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                                    >> 0x12U))
                                                         : 1U))) 
                                                     << 0xcU)) 
                                                 | (0xfffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                                               >> 6U)))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffU & (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT 
                                                         >> 0x11U))))) 
                    << 0x11U) | (QData)((IData)(((0x1fe00U 
                                                  & (((0xfU 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT 
                                                                  >> 0xdU))) 
                                                      * 
                                                      (0xfU 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT 
                                                                  >> 9U)))) 
                                                     << 9U)) 
                                                 | (0x1ffU 
                                                    & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT 
                = (((QData)((IData)(((0x8fU < (0x80U 
                                               ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193))) 
                                     | ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h9b071e30__0) 
                                        | (0x3dU > 
                                           (0x80U ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193))))))) 
                    << 0x22U) | (((QData)((IData)(vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_3_fOperands_S0_first___05F173_BIT_30_209_ETC___05F_d1212)) 
                                  << 0x21U) | (((QData)((IData)(
                                                                ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hf418fd2e__0)
                                                                  ? 0U
                                                                  : 0xfU))) 
                                                << 0x1dU) 
                                               | (QData)((IData)(
                                                                 ((((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hf418fd2e__0)
                                                                     ? 0U
                                                                     : 7U) 
                                                                   << 0x1aU) 
                                                                  | ((0xe0000U 
                                                                      & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                                                         << 0x11U)) 
                                                                     | (((0U 
                                                                          != 
                                                                          (0xfU 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                                                              >> 0x1aU))) 
                                                                         << 0x10U) 
                                                                        | ((0xe000U 
                                                                            & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                                                               >> 0xaU)) 
                                                                           | (((0U 
                                                                                != 
                                                                                (0xfU 
                                                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                                                                >> 0xcU))) 
                                                                               << 0xcU) 
                                                                              | ((0xe00U 
                                                                                & vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT) 
                                                                                | (((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193) 
                                                                                << 1U) 
                                                                                | (IData)(vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_3_fOperands_S0_first___05F173_BIT_30_209_ETC___05F_d1212)))))))))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                = (((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_OUT) 
                    << 0x11U) | (((IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_x) 
                                  << 3U) | (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_rmode)));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_ENQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT 
                = (((QData)((IData)((0x3ffffffU & (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                           >> 0x1aU))))) 
                    << 0x14U) | (QData)((IData)(((0xff000U 
                                                  & (((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                                  >> 0x33U)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                                  >> 0x12U))
                                                       : 
                                                      ((8U 
                                                        > 
                                                        (0x3fU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT)))
                                                        ? 
                                                       ((0xfeU 
                                                         & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_first___05F38_BITS_25_TO_18_45_ETC___05F_d648)) 
                                                        | (1U 
                                                           & ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_first___05F38_BITS_25_TO_18_45_ETC___05F_d648) 
                                                              | (0U 
                                                                 != 
                                                                 ((7U 
                                                                   >= 
                                                                   (0x3fU 
                                                                    & ((IData)(8U) 
                                                                       - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT))))
                                                                   ? 
                                                                  (0xffU 
                                                                   & ((IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                                               >> 0x12U)) 
                                                                      << 
                                                                      (0x3fU 
                                                                       & ((IData)(8U) 
                                                                          - (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT)))))
                                                                   : 0U)))))
                                                        : 
                                                       ((0U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                                     >> 0x12U))))
                                                         ? (IData)(
                                                                   (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                                    >> 0x12U))
                                                         : 1U))) 
                                                     << 0xcU)) 
                                                 | (0xfffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                                               >> 6U)))))));
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg = 1U;
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                = vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_IN;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg = 0U;
        }
        if (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_DEQ) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg = 1U;
        } else if (vlSelf->mktb_float__DOT__exp__DOT__rg_finish_EN) {
            vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg = 0U;
        }
    } else {
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__counter = 0U;
        vlSelf->mktb_float__DOT__rg_state = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S4__DOT__empty_reg = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S2_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S1_D_OUT = 0ULL;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_OUT = 0U;
        vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_OUT = 0U;
    }
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                             >> 0xcU)) - (0x3fU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                   >> 3U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                             >> 0x1aU)) - (0x3fU & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                            >> 0x11U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                             >> 0xcU)) - (0x3fU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                   >> 3U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                             >> 0x1aU)) - (0x3fU & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                            >> 0x11U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                             >> 0xcU)) - (0x3fU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                   >> 3U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                             >> 0x1aU)) - (0x3fU & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                            >> 0x11U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121 
        = (0x7fU & ((0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                     >> 6U))) - (0x3fU 
                                                 & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT))));
    mktb_float__DOT__exp__DOT__sfdin___05Fh1658 = (0xffU 
                                                   & ((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                                  >> 0xaU)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                                  >> 0xcU))
                                                       : (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                                  >> 0x14U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313 
        = (0x7fU & ((0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                     >> 6U))) - (0x3fU 
                                                 & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT))));
    mktb_float__DOT__exp__DOT__sfdin___05Fh4721 = (0xffU 
                                                   & ((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                                  >> 0xaU)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                                  >> 0xcU))
                                                       : (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                                  >> 0x14U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                             >> 0xcU)) - (0x3fU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                   >> 3U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592 
        = (0xffU & ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                             >> 0x1aU)) - (0x3fU & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                            >> 0x11U))));
    mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505 
        = (0x7fU & ((0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                     >> 6U))) - (0x3fU 
                                                 & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT))));
    mktb_float__DOT__exp__DOT__sfdin___05Fh7783 = (0xffU 
                                                   & ((1U 
                                                       & (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                                  >> 0xaU)))
                                                       ? (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                                  >> 0xcU))
                                                       : (IData)(
                                                                 (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                                  >> 0x14U))));
    vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697 
        = (0x7fU & ((0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                     >> 6U))) - (0x3fU 
                                                 & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT))));
    vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845 
        = (0xffU & ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                   >> 0xaU))) ? (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                                         >> 0xcU))
                     : (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S3_D_OUT 
                                >> 0x14U))));
    mktb_float__DOT__exp__DOT__IF_fpmul_2_fState_S3_first___05F051_BITS_8_TO_1_05_ETC___05F_d1086 
        = ((0x80U >= (0xffU & (0x80U ^ (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                >> 1U)))))
            ? 1U : (0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                    >> 1U))));
    mktb_float__DOT__exp__DOT__sfdlsb___05Fh17091 = 
        (0U != ((7U >= (0xffU & ((IData)(8U) - ((IData)(0xc2U) 
                                                - (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                           >> 1U))))))
                 ? (0xffU & ((IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                      >> 9U)) << (0xffU 
                                                  & ((IData)(8U) 
                                                     - 
                                                     ((IData)(0xc2U) 
                                                      - (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                                 >> 1U)))))))
                 : 0U));
    mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_first___05F051_BITS_16_TO_9_062___05FETC___05F_d1075 
        = ((7U >= (0xffU & (((IData)(0xc2U) - (IData)(
                                                      (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                       >> 1U))) 
                            - (IData)(1U)))) ? (0xffU 
                                                & ((0xffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                               >> 9U))) 
                                                   >> 
                                                   (0xffU 
                                                    & (((IData)(0xc2U) 
                                                        - (IData)(
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                                   >> 1U))) 
                                                       - (IData)(1U)))))
            : 0U);
    if ((0x81U >= (0xffU & (0x80U ^ ((IData)(0xc2U) 
                                     - (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                >> 1U))))))) {
        if ((1U == (0xffU & ((IData)(0xc2U) - (IData)(
                                                      (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                       >> 1U)))))) {
            mktb_float__DOT__exp__DOT__din_bias___05Fh18325 = 0x3eU;
            mktb_float__DOT__exp__DOT__sfdres___05Fh16868 
                = (0xffU & ((0x7eU & ((IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                               >> 0xbU)) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                             >> 0xaU)) 
                                                    | (IData)(mktb_float__DOT__exp__DOT__sfdlsb___05Fh17091)))));
        } else {
            mktb_float__DOT__exp__DOT__din_bias___05Fh18325 
                = ((0x80U >= (0xffU & (0x80U ^ (IData)(
                                                       (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                        >> 1U)))))
                    ? (0x3fU & ((IData)(1U) + (0xffU 
                                               & (- (IData)(
                                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                             >> 1U))))))
                    : 0U);
            mktb_float__DOT__exp__DOT__sfdres___05Fh16868 
                = (0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                    >> 9U)));
        }
    } else {
        mktb_float__DOT__exp__DOT__din_bias___05Fh18325 = 0x3fU;
        mktb_float__DOT__exp__DOT__sfdres___05Fh16868 
            = (0xffU & ((0xfeU & (IData)(mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_first___05F051_BITS_16_TO_9_062___05FETC___05F_d1075)) 
                        | (1U & ((IData)(mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_first___05F051_BITS_16_TO_9_062___05FETC___05F_d1075) 
                                 | (IData)(mktb_float__DOT__exp__DOT__sfdlsb___05Fh17091)))));
    }
    mktb_float__DOT__exp__DOT__IF_fpmul_3_fState_S3_first___05F251_BITS_8_TO_1_25_ETC___05F_d1286 
        = ((0x80U >= (0xffU & (0x80U ^ (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                >> 1U)))))
            ? 1U : (0xfU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                    >> 1U))));
    mktb_float__DOT__exp__DOT__sfdlsb___05Fh20277 = 
        (0U != ((7U >= (0xffU & ((IData)(8U) - ((IData)(0xc2U) 
                                                - (IData)(
                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                           >> 1U))))))
                 ? (0xffU & ((IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                      >> 9U)) << (0xffU 
                                                  & ((IData)(8U) 
                                                     - 
                                                     ((IData)(0xc2U) 
                                                      - (IData)(
                                                                (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                                 >> 1U)))))))
                 : 0U));
    mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_first___05F251_BITS_16_TO_9_262___05FETC___05F_d1275 
        = ((7U >= (0xffU & (((IData)(0xc2U) - (IData)(
                                                      (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                       >> 1U))) 
                            - (IData)(1U)))) ? (0xffU 
                                                & ((0xffU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                               >> 9U))) 
                                                   >> 
                                                   (0xffU 
                                                    & (((IData)(0xc2U) 
                                                        - (IData)(
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                                   >> 1U))) 
                                                       - (IData)(1U)))))
            : 0U);
    if ((0x81U >= (0xffU & (0x80U ^ ((IData)(0xc2U) 
                                     - (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                >> 1U))))))) {
        if ((1U == (0xffU & ((IData)(0xc2U) - (IData)(
                                                      (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                       >> 1U)))))) {
            mktb_float__DOT__exp__DOT__din_bias___05Fh21511 = 0x3eU;
            mktb_float__DOT__exp__DOT__sfdres___05Fh20054 
                = (0xffU & ((0x7eU & ((IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                               >> 0xbU)) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                             >> 0xaU)) 
                                                    | (IData)(mktb_float__DOT__exp__DOT__sfdlsb___05Fh20277)))));
        } else {
            mktb_float__DOT__exp__DOT__din_bias___05Fh21511 
                = ((0x80U >= (0xffU & (0x80U ^ (IData)(
                                                       (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                        >> 1U)))))
                    ? (0x3fU & ((IData)(1U) + (0xffU 
                                               & (- (IData)(
                                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                             >> 1U))))))
                    : 0U);
            mktb_float__DOT__exp__DOT__sfdres___05Fh20054 
                = (0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                    >> 9U)));
        }
    } else {
        mktb_float__DOT__exp__DOT__din_bias___05Fh21511 = 0x3fU;
        mktb_float__DOT__exp__DOT__sfdres___05Fh20054 
            = (0xffU & ((0xfeU & (IData)(mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_first___05F251_BITS_16_TO_9_262___05FETC___05F_d1275)) 
                        | (1U & ((IData)(mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_first___05F251_BITS_16_TO_9_262___05FETC___05F_d1275) 
                                 | (IData)(mktb_float__DOT__exp__DOT__sfdlsb___05Fh20277)))));
    }
    vlSelf->mktb_float__DOT__counter_D_IN = (0x3ffU 
                                             & ((IData)(1U) 
                                                + (IData)(vlSelf->mktb_float__DOT__counter)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg) 
           & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S4__DOT__empty_reg)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg) 
           & (~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S4__DOT__empty_reg)));
    mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33 
        = (((0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11)) 
            > (0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16))) 
           | (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11) 
               == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16)) 
              & ((((0U != (0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                   >> 0xcU))) << 6U) 
                  | (0x38U & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                              >> 6U))) > (((0U != (0xfU 
                                                   & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                      >> 0x1aU))) 
                                           << 6U) | 
                                          (0x38U & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                            >> 0x14U))))));
    mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225 
        = (((0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203)) 
            > (0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208))) 
           | (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203) 
               == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208)) 
              & ((((0U != (0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                   >> 0xcU))) << 6U) 
                  | (0x38U & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                              >> 6U))) > (((0U != (0xfU 
                                                   & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                      >> 0x1aU))) 
                                           << 6U) | 
                                          (0x38U & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                            >> 0x14U))))));
    mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417 
        = (((0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395)) 
            > (0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400))) 
           | (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395) 
               == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400)) 
              & ((((0U != (0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                   >> 0xcU))) << 6U) 
                  | (0x38U & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                              >> 6U))) > (((0U != (0xfU 
                                                   & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                      >> 0x1aU))) 
                                           << 6U) | 
                                          (0x38U & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                            >> 0x14U))))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h313e100d__0 
        = (IData)((0x40U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h3ff71dbe__0 
        = (((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
            >> 7U) & (0xfU == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121)));
    mktb_float__DOT__exp__DOT__IF_IF_fpadder_1_fState_S3_first___05F07_BIT_10_12___05FETC___05F_d159 
        = (0xfU & (((0x80U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                     ? 0U : ((0x40U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                              ? 1U : ((0x20U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                       ? 2U : ((0x10U 
                                                & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h245915ac__0 
        = (1U & ((~ ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658)))))));
    mktb_float__DOT__exp__DOT____VdfgTmp_he89ae0f2__0 
        = (IData)((0x40U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h93f67e5d__0 
        = (((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
            >> 7U) & (0xfU == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313)));
    mktb_float__DOT__exp__DOT__IF_IF_fpadder_2_fState_S3_first___05F99_BIT_10_04___05FETC___05F_d351 
        = (0xfU & (((0x80U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                     ? 0U : ((0x40U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                              ? 1U : ((0x20U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                       ? 2U : ((0x10U 
                                                & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h840cb8d5__0 
        = (1U & ((~ ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721)))))));
    mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609 
        = (((0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587)) 
            > (0x80U ^ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592))) 
           | (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587) 
               == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592)) 
              & ((((0U != (0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                   >> 0xcU))) << 6U) 
                  | (0x38U & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                              >> 6U))) > (((0U != (0xfU 
                                                   & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                      >> 0x1aU))) 
                                           << 6U) | 
                                          (0x38U & 
                                           (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                            >> 0x14U))))));
    mktb_float__DOT__exp__DOT____VdfgTmp_hddbfceca__0 
        = (IData)((0x40U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h1a440f5b__0 
        = (((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
            >> 7U) & (0xfU == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505)));
    mktb_float__DOT__exp__DOT__IF_IF_fpadder_3_fState_S3_first___05F91_BIT_10_96___05FETC___05F_d543 
        = (0xfU & (((0x80U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                     ? 0U : ((0x40U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                              ? 1U : ((0x20U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                       ? 2U : ((0x10U 
                                                & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h3dbc9c35__0 
        = (1U & ((~ ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783)))))));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_he325a082__0 
        = (IData)((0x40U == (0xc0U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hb011b47d__0 
        = (((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
            >> 7U) & (0xfU == (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697)));
    vlSelf->mktb_float__DOT__exp__DOT__IF_IF_fpadder_4_fState_S3_first___05F83_BIT_10_88___05FETC___05F_d735 
        = (0xfU & (((0x80U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                     ? 0U : ((0x40U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                              ? 1U : ((0x20U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                       ? 2U : ((0x10U 
                                                & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_ha31bf39d__0 
        = (1U & ((~ ((IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845)))))));
    mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087 
        = (((0x81U < (0xffU & (0x80U ^ ((IData)(0xc2U) 
                                        - (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                   >> 1U)))))) 
            | (1U == (0xffU & ((IData)(0xc2U) - (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                         >> 1U))))))
            ? 0U : (IData)(mktb_float__DOT__exp__DOT__IF_fpmul_2_fState_S3_first___05F051_BITS_8_TO_1_05_ETC___05F_d1086));
    mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287 
        = (((0x81U < (0xffU & (0x80U ^ ((IData)(0xc2U) 
                                        - (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                   >> 1U)))))) 
            | (1U == (0xffU & ((IData)(0xc2U) - (IData)(
                                                        (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                         >> 1U))))))
            ? 0U : (IData)(mktb_float__DOT__exp__DOT__IF_fpmul_3_fState_S3_first___05F251_BITS_8_TO_1_25_ETC___05F_d1286));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_DEQ)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_1_fState_S3_first___05F07_B_ETC___05F_d162 
        = ((0x40U | (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_1_fState_S3_first___05F07_BIT_10_12___05FETC___05F_d159)) 
           <= (0x7fU & (0x40U ^ ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121) 
                                 - (IData)(0x42U)))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h02ddd2cc__0 
        = (IData)(((0U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))) 
                   & (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h245915ac__0)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_2_fState_S3_first___05F99_B_ETC___05F_d354 
        = ((0x40U | (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_2_fState_S3_first___05F99_BIT_10_04___05FETC___05F_d351)) 
           <= (0x7fU & (0x40U ^ ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313) 
                                 - (IData)(0x42U)))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h8cfd2360__0 
        = (IData)(((0U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))) 
                   & (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h840cb8d5__0)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_3_fState_S3_first___05F91_B_ETC___05F_d546 
        = ((0x40U | (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_3_fState_S3_first___05F91_BIT_10_96___05FETC___05F_d543)) 
           <= (0x7fU & (0x40U ^ ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505) 
                                 - (IData)(0x42U)))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h78cf662a__0 
        = (IData)(((0U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))) 
                   & (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3dbc9c35__0)));
    vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_4_fState_S3_first___05F83_B_ETC___05F_d738 
        = ((0x40U | (IData)(vlSelf->mktb_float__DOT__exp__DOT__IF_IF_fpadder_4_fState_S3_first___05F83_BIT_10_88___05FETC___05F_d735)) 
           <= (0x7fU & (0x40U ^ ((IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fState_S3_first___05F83_BITS_9___05FETC___05F_d697) 
                                 - (IData)(0x42U)))));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h97c68da2__0 
        = (IData)(((0U == (0xc0U & (IData)(vlSelf->mktb_float__DOT__exp__DOT__sfdin___05Fh10845))) 
                   & (IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_ha31bf39d__0)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096 
        = (0x7fU & ((IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087) 
                    - (IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh18325)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h008074bd__0 
        = (IData)((0x40U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))));
    mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_2_fState_S3_first___05F051_B_ETC___05F_d1134 
        = (0xfU & (((0x80U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                     ? 0U : ((0x40U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                              ? 1U : ((0x20U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                       ? 2U : ((0x10U 
                                                & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h67405d8e__0 
        = (1U & ((~ ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868)))))));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296 
        = (0x7fU & ((IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287) 
                    - (IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh21511)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h7036bbee__0 
        = (IData)((0x40U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))));
    mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_3_fState_S3_first___05F251_B_ETC___05F_d1334 
        = (0xfU & (((0x80U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                     ? 0U : ((0x40U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                              ? 1U : ((0x20U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                       ? 2U : ((0x10U 
                                                & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                ? 3U
                                                : (
                                                   (8U 
                                                    & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                    ? 4U
                                                    : 
                                                   ((4U 
                                                     & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                     ? 5U
                                                     : 
                                                    ((2U 
                                                      & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                      ? 6U
                                                      : 
                                                     ((1U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                       ? 7U
                                                       : 8U)))))))) 
                   - (IData)(1U)));
    mktb_float__DOT__exp__DOT____VdfgTmp_he3763359__0 
        = (1U & ((~ ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
                     >> 5U)) & (~ (IData)((0U != (0x1fU 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054)))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S3_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S4_D_IN 
        = (((IData)((0x800ULL == (0x200000000800ULL 
                                  & vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                       >> 0x2dU))) ? 0U
                         : ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3ff71dbe__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                  ? 
                                                 ((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                               >> 6U))))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(
                                                             (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                              >> 6U))))
                                                  : 
                                                 ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h313e100d__0)
                                                   ? 
                                                  ((0U 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                                >> 6U))))
                                                    ? 1U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                               >> 6U)))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_1_fState_S3_first___05F07_B_ETC___05F_d162)) 
                                                       | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h02ddd2cc__0)))
                                                    ? 0U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT 
                                                               >> 6U))))))) 
                             << 9U) | ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3ff71dbe__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h313e100d__0)
                                                        ? 
                                                       ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h02ddd2cc__0)
                                                         ? (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658)
                                                         : 
                                                        (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_1_fState_S3_first___05F07_B_ETC___05F_d162)
                                                           ? 
                                                          ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
                                                           << (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_1_fState_S3_first___05F07_BIT_10_12___05FETC___05F_d159))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fState_S3_first___05F07_BITS_9___05FETC___05F_d121) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh1658))) 
                                                                 | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h245915ac__0))))
                                                      ? (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT)
                                                      : 
                                                     ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_1_fState_S3_first___05F07_B_ETC___05F_d162)
                                                       ? 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S3_D_OUT) 
                                                       + (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_1_fState_S3_first___05F07_BIT_10_12___05FETC___05F_d159))
                                                       : 0x3fU)))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_IN 
        = (((QData)((IData)((7U & vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT))) 
            << 0x22U) | (((QData)((IData)(((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33)
                                            ? (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                   >> 0xcU)))
                                            : (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                   >> 0x1aU)))))) 
                          << 0x20U) | (QData)((IData)(
                                                      ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33)
                                                          ? 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                          >> 9U)
                                                          : 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                          >> 0x17U)) 
                                                        << 0x1dU) 
                                                       | ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33)
                                                             ? 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                 >> 0x1aU)))
                                                             : 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                 >> 0xcU)))) 
                                                           << 0x18U) 
                                                          | ((0xe00000U 
                                                              & (((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33)
                                                                   ? 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                   >> 0x17U)
                                                                   : 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                   >> 9U)) 
                                                                 << 0x15U)) 
                                                             | ((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_1_fOperands_S0_first_BITS_ETC___05F_d33)
                                                                 ? 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                     << 1U)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                          >> 0x10U)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                          >> 0x1eU))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                              << 3U)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16)))))))
                                                                 : 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                     >> 0xdU)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                          >> 0x1eU)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                          >> 0x10U))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                           >> 0xeU)) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fOperands_S0_D_OUT 
                                                                              >> 0xbU)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_29___05FETC___05F_d16) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_1_fOperands_S0_first_BITS_15___05FETC___05F_d11)))))))))))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_IN 
        = (((QData)((IData)((7U & vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT))) 
            << 0x22U) | (((QData)((IData)(((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225)
                                            ? (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                   >> 0xcU)))
                                            : (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                   >> 0x1aU)))))) 
                          << 0x20U) | (QData)((IData)(
                                                      ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225)
                                                          ? 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                          >> 9U)
                                                          : 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                          >> 0x17U)) 
                                                        << 0x1dU) 
                                                       | ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225)
                                                             ? 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                 >> 0x1aU)))
                                                             : 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                 >> 0xcU)))) 
                                                           << 0x18U) 
                                                          | ((0xe00000U 
                                                              & (((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225)
                                                                   ? 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                   >> 0x17U)
                                                                   : 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                   >> 9U)) 
                                                                 << 0x15U)) 
                                                             | ((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_2_fOperands_S0_first___05F97___05FETC___05F_d225)
                                                                 ? 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                     << 1U)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                          >> 0x10U)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                          >> 0x1eU))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                              << 3U)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208)))))))
                                                                 : 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                     >> 0xdU)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                          >> 0x1eU)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                          >> 0x10U))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                           >> 0xeU)) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fOperands_S0_D_OUT 
                                                                              >> 0xbU)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d208) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fOperands_S0_first___05F97_BITS_ETC___05F_d203)))))))))))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_IN 
        = (((QData)((IData)((7U & vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT))) 
            << 0x22U) | (((QData)((IData)(((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417)
                                            ? (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                   >> 0xcU)))
                                            : (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                   >> 0x1aU)))))) 
                          << 0x20U) | (QData)((IData)(
                                                      ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417)
                                                          ? 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                          >> 9U)
                                                          : 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                          >> 0x17U)) 
                                                        << 0x1dU) 
                                                       | ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417)
                                                             ? 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                 >> 0x1aU)))
                                                             : 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                 >> 0xcU)))) 
                                                           << 0x18U) 
                                                          | ((0xe00000U 
                                                              & (((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417)
                                                                   ? 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                   >> 0x17U)
                                                                   : 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                   >> 9U)) 
                                                                 << 0x15U)) 
                                                             | ((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_3_fOperands_S0_first___05F89___05FETC___05F_d417)
                                                                 ? 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                     << 1U)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                          >> 0x10U)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                          >> 0x1eU))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                              << 3U)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400)))))))
                                                                 : 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                     >> 0xdU)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                          >> 0x1eU)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                          >> 0x10U))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                           >> 0xeU)) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fOperands_S0_D_OUT 
                                                                              >> 0xbU)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d400) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fOperands_S0_first___05F89_BITS_ETC___05F_d395)))))))))))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_IN 
        = (((QData)((IData)((7U & vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT))) 
            << 0x22U) | (((QData)((IData)(((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609)
                                            ? (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                   >> 0xcU)))
                                            : (0U != 
                                               (0xfU 
                                                & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                   >> 0x1aU)))))) 
                          << 0x20U) | (QData)((IData)(
                                                      ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609)
                                                          ? 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                          >> 9U)
                                                          : 
                                                         (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                          >> 0x17U)) 
                                                        << 0x1dU) 
                                                       | ((((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609)
                                                             ? 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                 >> 0x1aU)))
                                                             : 
                                                            (0U 
                                                             != 
                                                             (0xfU 
                                                              & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                 >> 0xcU)))) 
                                                           << 0x18U) 
                                                          | ((0xe00000U 
                                                              & (((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609)
                                                                   ? 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                   >> 0x17U)
                                                                   : 
                                                                  (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                   >> 9U)) 
                                                                 << 0x15U)) 
                                                             | ((IData)(mktb_float__DOT__exp__DOT__NOT_0_CONCAT_fpadder_4_fOperands_S0_first___05F81___05FETC___05F_d609)
                                                                 ? 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                     << 1U)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                          >> 0x10U)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                          >> 0x1eU))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                              << 3U)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592)))))))
                                                                 : 
                                                                ((0x20000U 
                                                                  & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                     >> 0xdU)) 
                                                                 | ((((1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                          >> 0x1eU)) 
                                                                      != 
                                                                      (1U 
                                                                       & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                          >> 0x10U))) 
                                                                     << 0x10U) 
                                                                    | ((0xf000U 
                                                                        & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                           >> 0xeU)) 
                                                                       | ((0xfc0U 
                                                                           & (vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fOperands_S0_D_OUT 
                                                                              >> 0xbU)) 
                                                                          | (0x3fU 
                                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d592) 
                                                                                - (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_4_fOperands_S0_first___05F81_BITS_ETC___05F_d587)))))))))))))));
    mktb_float__DOT__exp__DOT____VdfgTmp_hdf31fb6e__0 
        = (((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
            >> 7U) & (0xfU == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_2_fState_S3_fir_ETC___05F_d1137 
        = ((0x40U | (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_2_fState_S3_first___05F051_B_ETC___05F_d1134)) 
           <= (0x7fU & (0x40U ^ ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096) 
                                 - (IData)(0x42U)))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h5a4b53af__0 
        = (IData)(((0U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))) 
                   & (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h67405d8e__0)));
    mktb_float__DOT__exp__DOT____VdfgTmp_h65459769__0 
        = (((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
            >> 7U) & (0xfU == (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296)));
    mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_3_fState_S3_fir_ETC___05F_d1337 
        = ((0x40U | (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_3_fState_S3_first___05F251_B_ETC___05F_d1334)) 
           <= (0x7fU & (0x40U ^ ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296) 
                                 - (IData)(0x42U)))));
    mktb_float__DOT__exp__DOT____VdfgTmp_h3324af5c__0 
        = (IData)(((0U == (0xc0U & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))) 
                   & (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_he3763359__0)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_first___05F2_BITS_25_TO_18_9_S_ETC___05F_d72 
        = ((7U >= (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT)))
            ? (0xffU & ((0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT 
                                          >> 0x12U))) 
                        >> (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_1_fState_S1_D_OUT))))
            : 0U);
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_first___05F54_BITS_25_TO_18_61_ETC___05F_d264 
        = ((7U >= (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT)))
            ? (0xffU & ((0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT 
                                          >> 0x12U))) 
                        >> (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S1_D_OUT))))
            : 0U);
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_first___05F46_BITS_25_TO_18_53_ETC___05F_d456 
        = ((7U >= (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT)))
            ? (0xffU & ((0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT 
                                          >> 0x12U))) 
                        >> (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S1_D_OUT))))
            : 0U);
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_first___05F38_BITS_25_TO_18_45_ETC___05F_d648 
        = ((7U >= (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT)))
            ? (0xffU & ((0xffU & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT 
                                          >> 0x12U))) 
                        >> (0x3fU & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_4_fState_S1_D_OUT))))
            : 0U);
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S4_D_IN 
        = (((IData)((1ULL == (0x400000001ULL & vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                       >> 0x22U))) ? 0U
                         : ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_hdf31fb6e__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                  ? 
                                                 ((0U 
                                                   == (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087)))
                                                  : 
                                                 ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h008074bd__0)
                                                   ? 
                                                  ((0U 
                                                    == (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087))
                                                    ? 1U
                                                    : (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_2_fState_S3_first___05F051___05FETC___05F_d1087))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_2_fState_S3_fir_ETC___05F_d1137)) 
                                                       | ((0x81U 
                                                           < 
                                                           (0xffU 
                                                            & (0x80U 
                                                               ^ 
                                                               ((IData)(0xc2U) 
                                                                - (IData)(
                                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                                           >> 1U)))))) 
                                                          | ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h5a4b53af__0) 
                                                             | (1U 
                                                                == 
                                                                (0xffU 
                                                                 & ((IData)(0xc2U) 
                                                                    - (IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fState_S3_D_OUT 
                                                                               >> 1U)))))))))
                                                    ? 0U
                                                    : (IData)(mktb_float__DOT__exp__DOT__IF_fpmul_2_fState_S3_first___05F051_BITS_8_TO_1_05_ETC___05F_d1086)))))) 
                             << 9U) | ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_hdf31fb6e__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h008074bd__0)
                                                        ? 
                                                       ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h5a4b53af__0)
                                                         ? (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868)
                                                         : 
                                                        (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_2_fState_S3_fir_ETC___05F_d1137)
                                                           ? 
                                                          ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
                                                           << (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_2_fState_S3_first___05F051_B_ETC___05F_d1134))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_2_fState_S3_fi_ETC___05F_d1096) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh16868))) 
                                                                 | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h67405d8e__0))))
                                                      ? (IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh18325)
                                                      : 
                                                     ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_2_fState_S3_fir_ETC___05F_d1137)
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh18325) 
                                                       + (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_2_fState_S3_first___05F051_B_ETC___05F_d1134))
                                                       : 0x3fU)))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S4_D_IN 
        = (((IData)((1ULL == (0x400000001ULL & vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                       >> 0x22U))) ? 0U
                         : ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h65459769__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                  ? 
                                                 ((0U 
                                                   == (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287)))
                                                  : 
                                                 ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h7036bbee__0)
                                                   ? 
                                                  ((0U 
                                                    == (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287))
                                                    ? 1U
                                                    : (IData)(mktb_float__DOT__exp__DOT__IF_NOT_194_MINUS_fpmul_3_fState_S3_first___05F251___05FETC___05F_d1287))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_3_fState_S3_fir_ETC___05F_d1337)) 
                                                       | ((0x81U 
                                                           < 
                                                           (0xffU 
                                                            & (0x80U 
                                                               ^ 
                                                               ((IData)(0xc2U) 
                                                                - (IData)(
                                                                          (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                                           >> 1U)))))) 
                                                          | ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3324af5c__0) 
                                                             | (1U 
                                                                == 
                                                                (0xffU 
                                                                 & ((IData)(0xc2U) 
                                                                    - (IData)(
                                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fState_S3_D_OUT 
                                                                               >> 1U)))))))))
                                                    ? 0U
                                                    : (IData)(mktb_float__DOT__exp__DOT__IF_fpmul_3_fState_S3_first___05F251_BITS_8_TO_1_25_ETC___05F_d1286)))))) 
                             << 9U) | ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h65459769__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h7036bbee__0)
                                                        ? 
                                                       ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3324af5c__0)
                                                         ? (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054)
                                                         : 
                                                        (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_3_fState_S3_fir_ETC___05F_d1337)
                                                           ? 
                                                          ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
                                                           << (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_3_fState_S3_first___05F251_B_ETC___05F_d1334))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_NOT_194_MINUS_fpmul_3_fState_S3_fi_ETC___05F_d1296) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(mktb_float__DOT__exp__DOT__sfdres___05Fh20054))) 
                                                                 | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_he3763359__0))))
                                                      ? (IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh21511)
                                                      : 
                                                     ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_194_MINUS_fpmul_3_fState_S3_fir_ETC___05F_d1337)
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__din_bias___05Fh21511) 
                                                       + (IData)(mktb_float__DOT__exp__DOT__IF_IF_194_MINUS_fpmul_3_fState_S3_first___05F251_B_ETC___05F_d1334))
                                                       : 0x3fU)))))));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S2_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_FULL_N 
        = (1U & ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1__DOT__empty_reg)) 
                 | (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_DEQ)));
    vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_2_fOperands_S0_first___05F73_BIT_30_009___05FETC___05F_d1012 
        = ((1U & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                  >> 0x1eU)) != (1U & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                       >> 0x10U)));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_haea742db__0 
        = ((IData)((0U == (0x3f800000U & vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT))) 
           | (IData)((0U == (0xfe00U & vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT))));
    vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993 
        = (0xffU & (((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                              >> 0x1aU)) - (0x3fU & 
                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                             >> 0x11U))) 
                    + ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                >> 0xcU)) - (0x3fU 
                                             & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_2_fOperands_S0_D_OUT 
                                                >> 3U)))));
    vlSelf->mktb_float__DOT__exp__DOT__NOT_fpmul_3_fOperands_S0_first___05F173_BIT_30_209_ETC___05F_d1212 
        = ((1U & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                  >> 0x1eU)) != (1U & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                       >> 0x10U)));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h9b071e30__0 
        = ((IData)((0U == (0x3f800000U & vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT))) 
           | (IData)((0U == (0xfe00U & vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT))));
    vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193 
        = (0xffU & (((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                              >> 0x1aU)) - (0x3fU & 
                                            (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                             >> 0x11U))) 
                    + ((0xfU & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                >> 0xcU)) - (0x3fU 
                                             & (vlSelf->mktb_float__DOT__exp__DOT__fpmul_3_fOperands_S0_D_OUT 
                                                >> 3U)))));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_1_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0_DEQ 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fOperands_S0__DOT__empty_reg) 
           & (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpmul_4_fState_S1_FULL_N));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h42c3b394__0 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_haea742db__0) 
           | (0x8fU >= (0x80U ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_2_fOperands_S0_first___05F73_BITS_2_ETC___05F_d993))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S4_D_IN 
        = (((IData)((0x800ULL == (0x200000000800ULL 
                                  & vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                       >> 0x2dU))) ? 0U
                         : ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h93f67e5d__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                  ? 
                                                 ((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                               >> 6U))))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(
                                                             (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                              >> 6U))))
                                                  : 
                                                 ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_he89ae0f2__0)
                                                   ? 
                                                  ((0U 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                                >> 6U))))
                                                    ? 1U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                               >> 6U)))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_2_fState_S3_first___05F99_B_ETC___05F_d354)) 
                                                       | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h8cfd2360__0)))
                                                    ? 0U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT 
                                                               >> 6U))))))) 
                             << 9U) | ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h93f67e5d__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_he89ae0f2__0)
                                                        ? 
                                                       ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h8cfd2360__0)
                                                         ? (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721)
                                                         : 
                                                        (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_2_fState_S3_first___05F99_B_ETC___05F_d354)
                                                           ? 
                                                          ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
                                                           << (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_2_fState_S3_first___05F99_BIT_10_04___05FETC___05F_d351))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_2_fState_S3_first___05F99_BITS_9___05FETC___05F_d313) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh4721))) 
                                                                 | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h840cb8d5__0))))
                                                      ? (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT)
                                                      : 
                                                     ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_2_fState_S3_first___05F99_B_ETC___05F_d354)
                                                       ? 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_2_fState_S3_D_OUT) 
                                                       + (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_2_fState_S3_first___05F99_BIT_10_04___05FETC___05F_d351))
                                                       : 0x3fU)))))));
    vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_hf418fd2e__0 
        = ((IData)(vlSelf->mktb_float__DOT__exp__DOT____VdfgTmp_h9b071e30__0) 
           | (0x8fU >= (0x80U ^ (IData)(vlSelf->mktb_float__DOT__exp__DOT___0_CONCAT_fpmul_3_fOperands_S0_first___05F173_BITS___05FETC___05F_d1193))));
    vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S4_D_IN 
        = (((IData)((0x800ULL == (0x200000000800ULL 
                                  & vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT))) 
            << 0xdU) | ((1U & (IData)((vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                       >> 0x2dU))) ? 0U
                         : ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h1a440f5b__0)
                               ? 0xfU : (0xfU & ((0x80U 
                                                  & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                  ? 
                                                 ((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                               >> 6U))))
                                                   ? 2U
                                                   : 
                                                  ((IData)(1U) 
                                                   + (IData)(
                                                             (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                              >> 6U))))
                                                  : 
                                                 ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_hddbfceca__0)
                                                   ? 
                                                  ((0U 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                                >> 6U))))
                                                    ? 1U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                               >> 6U)))
                                                   : 
                                                  ((1U 
                                                    & ((~ (IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_3_fState_S3_first___05F91_B_ETC___05F_d546)) 
                                                       | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h78cf662a__0)))
                                                    ? 0U
                                                    : (IData)(
                                                              (vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT 
                                                               >> 6U))))))) 
                             << 9U) | ((((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h1a440f5b__0)
                                          ? 7U : (7U 
                                                  & (((0x80U 
                                                       & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))
                                                       ? 
                                                      ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
                                                       << 1U)
                                                       : 
                                                      ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_hddbfceca__0)
                                                        ? 
                                                       ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
                                                        << 2U)
                                                        : 
                                                       ((IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h78cf662a__0)
                                                         ? (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783)
                                                         : 
                                                        (((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_3_fState_S3_first___05F91_B_ETC___05F_d546)
                                                           ? 
                                                          ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
                                                           << (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_3_fState_S3_first___05F91_BIT_10_96___05FETC___05F_d543))
                                                           : 
                                                          ((7U 
                                                            >= 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505) 
                                                                - (IData)(0x42U))))
                                                            ? 
                                                           ((IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783) 
                                                            << 
                                                            (0x7fU 
                                                             & ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_fpadder_3_fState_S3_first___05F91_BITS_9___05FETC___05F_d505) 
                                                                - (IData)(0x42U))))
                                                            : 0U)) 
                                                         << 2U)))) 
                                                     >> 5U))) 
                                        << 6U) | (0x3fU 
                                                  & ((1U 
                                                      & (IData)(
                                                                ((0U 
                                                                  != 
                                                                  (0xc0U 
                                                                   & (IData)(mktb_float__DOT__exp__DOT__sfdin___05Fh7783))) 
                                                                 | (IData)(mktb_float__DOT__exp__DOT____VdfgTmp_h3dbc9c35__0))))
                                                      ? (IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT)
                                                      : 
                                                     ((IData)(mktb_float__DOT__exp__DOT___0_CONCAT_IF_IF_fpadder_3_fState_S3_first___05F91_B_ETC___05F_d546)
                                                       ? 
                                                      ((IData)(vlSelf->mktb_float__DOT__exp__DOT__fpadder_3_fState_S3_D_OUT) 
                                                       + (IData)(mktb_float__DOT__exp__DOT__IF_IF_fpadder_3_fState_S3_first___05F91_BIT_10_96___05FETC___05F_d543))
                                                       : 0x3fU)))))));
}
