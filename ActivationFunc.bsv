package ActivationFunc;
import Floatingpoint::*;
import ClientServer::*;
import GetPut::*;



/* function FloatingPoint#(4,3,6) int_lut (bit#(4) x);
    
      case(x) 
              begin 
              4'b0000 : return unpack('b00001000000001); 
              4'b0001 : return unpack('b00000011000000); 
              4'b0010 : return unpack('b00001001000100); 
              4'b0011 : return unpack('b00001101000110);  
              4'b0100 : return unpack('b00010001001000);
              4'b0101 : return unpack('b00010110001010);
              4'b0110 : return unpack('b00011010001100);
              4'b0111 : return unpack('b00011111001110);
              4'b1000 : return unpack('b00100011010000);
              4'b1001 : return unpack('b00101000010010);
              4'b1010 : return unpack('b00101100010100);
              4'b1011 : return unpack('b00110001010110);
              4'b1100 : return unpack('b00110101011000);
              4'b1101 : return unpack('b00111001011010);
              4'b1110 : return unpack('b00111110011100);
              4'b1111 : return unpack('b01000010011110);
                        
             end 

 endfunction 

function FloatingPoint#(4,3,6) fac_lut (Bit#(3) x);
          
          case(x) 
              begin 
               3'b000 : return unpack('b00000000000000);
               3'b001 : return unpack('b01111110010000);
               3'b010 : return unpack('b01111100010000);
               3'b011 : return unpack('b01111011010000);
               3'b100 : return unpack('b01111010010000);
               3'b101 : return unpack('b01111001010000);
               3'b110 : return unpack('b01111111010001);
               3'b111 : return unpack('b01111101010001);
           
             end 
           

endfunction*/



/*module mkFloatingPointExp((Server#(Tuple3#(FloatingPoint#(e,m,b), RoundMode), FloatingPoint#(e,m,b))))

  
  
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder <- mkFloatingPointAdder();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_1 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_2 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_3 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_4 <- mkFloatingPointMultiplier();
  
  ////////////////////////////////////////////////////////////////////////////////
   /// S0
   ////////////////////////////////////////////////////////////////////////////////
  
   FIFO#(Tuple3#(FloatingPoint#(e,m,b),
		 RoundMode))                 fexpOperands_S0        <- mkLFIFO;




 ////////////////////////////////////////////////////////////////////////////////
   /// S1 
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple5#(CommonState#(e,m,b),
		 Bit#(TAdd#(e,1)),
		 Bit#(TAdd#(e,1)),,
		 Bit#(7)) fState_S1 <- mkLFIFO;

   rule s1_stage;
      match { .opA,.rmode } <- toGet(fOperands_S0).get;
      
      CommonState#(e,m,b) s = CommonState {
	 res: tagged Invalid,
	 rmode: rmode
	 };

    Int#(TAdd#(b,2)) expA = zeroExtend(unpack(opA.exp)) - zeroExtend(unpack(opA.bias));
     Bit#(TAdd#(m,1)) opAsfd = { getHiddenBit(opA), opA.sfd };
     
     let temp_1 = opA.exp;
     let temp_2 = opA.exp;
     Bit#(7) lv_tmp = 0;   
     
      if  ( expA >= 4 && opA.sign == 0) begin 
        FloatingPoint#(e,m,b) out;        
           out.sign = opA.sign ;
	   out.exp = 0;
	   out.sfd = 0;
           out.bias = 0;
           
            s.res = tagged Valid out;
          end 
          
          
       else if ( (expA < 4 && expA > -2 ) && rg_x.sign == 0) begin  
                    
                    if ( expA > 0) begin 
                        Bit#(7) lv_tmp = opAsfd << expA;end  
                          else begin 
                             Bit#(7) lv_tmp = opAsfd ; end
                                                

                  else if  (exp < -2 && rg_x.sign == 1 ) begin                       
                         let temp_1 = opA.exp >> 4
                         let temp_2 = opA.exp >> 2 
                        end
                        
                  fState_S1.enq(tuple5(s,
		           temp_1,
			   temp_2,
			   lv_tmp));   
               endrule         
   ////////////////////////////////////////////////////////////////////////////////
   /// S2
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple4#(CommonState#(e,m,b),
		)) fState_S2 <- mkLFIFO;    
		 
		 
    rule s2_stage;
        match {.s, .temp_1, .temp_2, .lv_tmp} <- toGet(fState_S1).get;  
          FloatingPoint#(e,m,b) out_1;        
           out.sign = opA.sign ;
	   out.exp = temp_1;
	   out.sfd = opA.sfd ;
           out.bias = opA.bias ;
           
           FloatingPoint#(e,m,b) out_2;        
           out.sign = opA.sign ;
	   out.exp = temp_2;
	   out.sfd = opA.sfd ;
           out.bias = opA.bias ;   
                    
          let  lv_int =  lv_tmp[7:3];
          let  lv_frac =  lv_tmp[2:0];
          let temp_3 =  int_lut(lv_int);
          let temp_4 =  fac_lut(lv_frac);
          
           fpmul_1.request.put (temp_3,temp_4,Rnd_Nearest_Even);
           fpadder.request.put (out_1,out_2,Rnd_Nearest_Even);  
           
           fState_S2.enq(tuple4(s));          
        endrule 
        
    ////////////////////////////////////////////////////////////////////////////////
   /// S3
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple4#(CommonState#(e,m,b),
		)) fState_S3 <- mkLFIFO;  

   rule s3_stage;
        
       let temp_0 <- fmul_1.response.get(); 
      
       let temp_5 = opA.exp >> 1;
       let temp_4 <- fpadder.response.get();
       let temp_4_1 = ~temp_4.sign; 
        fpmul_2.request.put(tuple3(temp_4_1,temp_3,Rnd_Nearest_Even));
        
         let x <- toGet(fState_S2).get;
      fState_S3.enq(x);
       
   endrule
   
    ////////////////////////////////////////////////////////////////////////////////
   /// S4
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(FloatingPoint#(e,m,b)) fState_S4 <- mkLFIFO;
   
   rule s4_stage;
                    let temp_5 <- fpmul_2.response.get();
                     let temp_5_1 = ~temp_5.sign;
                      
                    fpmul_3.request.put(tuple3(temp_5_1,opA,Rnd_Nearest_Even));
   endrule	
   
   ////////////////////////////////////////////////////////////////////////////////
   /// S5
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(FloatingPoint#(e,m,b)) fState_S4 <- mkLFIFO;
   
   rule s5_stage;
                    let temp_6 <- fpmul_3.response.get();
                     let temp_7 =~temp_6.sign;
                    fpmul_4.request.put (temp_7,temp_0,Rnd_Nearest_Even);
                          
                      
                 
   endrule
   
   interface request = toPut(fOperands_S0);
   interface response = toGet(fState_S4);
   
   	 
endmodule: mkFloatingPointMultiplier */

 interface Ifc_FloatingPointExp;            
      method Action ma_start  (FloatingPoint#(4,3,6)  x, RoundMode rmode);
      method ActionValue#(FloatingPoint#(4,3,6)) mav_out();
       //method FloatingPoint#(4,3,6) mv_out(); 	      
   endinterface 
(*synthesize*)
module mkFloatingPointExp(Ifc_FloatingPointExp); 

    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder_1 <- mkFloatingPointAdder();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder_2 <- mkFloatingPointAdder();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder_3 <- mkFloatingPointAdder();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder_4 <- mkFloatingPointAdder();
    
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_1 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_2 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_3 <- mkFloatingPointMultiplier();
    Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul_4 <- mkFloatingPointMultiplier();
 
      Reg#(FloatingPoint#(4,3,6)) rg_x  <- mkRegA(unpack(0));
      Reg#(RoundMode) rg_rmode  <- mkRegA(unpack(0));
      Reg#(FloatingPoint#(4,3,6)) rg_out  <- mkRegA(unpack(0));
      Reg#(FloatingPoint#(4,3,6)) rg_precise_1  <- mkRegA(unpack(0));
      Reg#(FloatingPoint#(4,3,6)) rg_precise_2  <- mkRegA(unpack(0));
      Reg#(FloatingPoint#(4,3,6)) rg_imprecise  <- mkRegA(unpack(0));
      
       Reg#(Bit#(1)) rg_start  <- mkRegA(0);
      Reg#(Bit#(1)) rg_finish  <- mkRegA(0);
      
     Int#(8) x_exp = zeroExtend(unpack(rg_x.exp)) - zeroExtend(unpack(rg_x.bias));
     Bit#(4) xsfd = { getHiddenBit(rg_x), rg_x.sfd };

    /*rule compute_exp_saturate ( x_exp >= 4 && rg_x.sign == False );  
        
             FloatingPoint#(4,3,6) out_1;        
	     out_1.sign = rg_x.sign ;
	     out_1.exp = 0;
	     out_1.sfd =  0;
	     out_1.bias = 0;
            
         rg_out <= out_1; 
        // $display("%d:compute_exp_saturate", x_exp);
          endrule*/  
          
          
        /*rule compute_exp_precise_stage_1 (x_exp < 4 && x_exp > -2);  
                    
                    if ( x_exp > 0) begin 
                        Bit#(7) lv_tmp = xsfd << x_exp;end  
                          else begin 
                             Bit#(7) lv_tmp = xsfd ; end
                             
                             let  lv_int =  lv_tmp[7:3];
                             let  lv_frac =  lv_tmp[2:0];
                                                
               fpmul_1.request.put(tuple3(int_lut(lv_int),fac_lut(lv_frac),rmode));            
         endrule  
         
          
           rule compute_exp_precise_stage_2; 
           
           
           
           
           endrule*/   
                  
                                                    
        rule compute_exp_imprecise_stage_1( /* x_exp < -2*/ x_exp < 4  /*&& rg_x.sign == True*/ );    
               //$display("%d:compute_exp_imprecise_stage_1",x_exp);              
                    
            FloatingPoint#(4,3,6) out_1;        
	     out_1.sign = rg_x.sign ;
	     out_1.exp = rg_x.exp;
	     out_1.sfd = rg_x.sfd;
	     out_1.bias = rg_x.bias + 4;
            
           FloatingPoint#(4,3,6) out_2;        
             out_2.sign = rg_x.sign ;
	     out_2.exp = rg_x.exp   ;
	     out_2.sfd = rg_x.sfd;
             out_2.bias = rg_x.bias + 2;
           
                    fpadder_1.request.put(tuple3(out_1,out_2,rg_rmode));   // first adder  input 
                    
                   //   $display("%b: out_1",out_1);
                   //    $display("%b: out_2",out_2);
                endrule                
              
         rule compute_exp_imprecise_stage_2 ;                
                        
              let fpadder_1_out<- fpadder_1.response.get();     // first adder  output 
                     
              FloatingPoint#(4,3,6) out_3;        
	       out_3.sign = !fpadder_1_out.sign ;
	       out_3.exp = fpadder_1_out.exp;
	       out_3.sfd = fpadder_1_out.sfd;
	       out_3.bias = fpadder_1_out.bias;  
	       
	       FloatingPoint#(4,3,6) out_one ;        
	       out_one.sign = False;
	       out_one.exp  = 4'b0010  ;
	       out_one.sfd  = 3'b000  ;
	       out_one.bias = 6'b000010 ;
	       
	      fpadder_2.request.put(tuple3(out_one,out_3,rg_rmode));      // minus one input  
	      
	   endrule    
	       
         rule compute_exp_imprecise_stage_3  ; 
              
             FloatingPoint#(4,3,6) out_4;        
	      out_4.sign = rg_x.sign  ;
	      out_4.exp = rg_x.exp;
	      out_4.sfd = rg_x.sfd;
	      out_4.bias = rg_x.bias + 1;
            
            
                   let fpadder_2_out <- fpadder_2.response.get();       // minus one output  
                         
                    fpmul_2.request.put(tuple3(fpadder_2_out,out_4,rg_rmode));  // first multi input
                                      
                endrule    
                       
          rule compute_exp_imprecise_stage_4;  
                     
                let temp_5 <- fpmul_2.response.get();     // first multi output
                     
                 FloatingPoint#(4,3,6) out_5;        
	          out_5.sign = !temp_5.sign ;
	          out_5.exp = temp_5.exp;
	          out_5.sfd = temp_5.sfd;
	          out_5.bias = temp_5.bias; 
	          
	        FloatingPoint#(4,3,6) out_one ;        
	         out_one.sign = False;
	         out_one.exp  = 4'b0010  ;
	         out_one.sfd  = 3'b000  ;
	         out_one.bias = 6'b000010 ;
	          
                 fpadder_3.request.put(tuple3(out_one,out_5,rg_rmode));   // minus one input     
               
               endrule 
                  
           rule compute_exp_imprecise_stage_5 ;   
                 
                 
                 let fpadder_3_out  <- fpadder_3.response.get();   // minus one output
                        
                    fpmul_3.request.put(tuple3(fpadder_3_out,rg_x,rg_rmode)); // second multi input
                    
                 
                     //  $display("%b: rg_x",rg_x);                                               
             endrule    
                      
                     
            rule compute_exp_imprecise_stage_6;
                   
                    let temp_6 <- fpmul_3.response.get(); // second multi output
                    
                 FloatingPoint#(4,3,6) out_6;        
	           out_6.sign = !temp_6.sign ;
	           out_6.exp = temp_6.exp;
	           out_6.sfd = temp_6.sfd;
	           out_6.bias = temp_6.bias;
	           
	        FloatingPoint#(4,3,6) out_one ;        
	         out_one.sign = False;
	         out_one.exp  = 4'b0010  ;
	         out_one.sfd  = 3'b000  ;
	         out_one.bias = 6'b000010 ;
	          
                 fpadder_4.request.put(tuple3(out_one,out_6,rg_rmode));  
	           
                                          
            endrule   
                
                  rule compute_exp_final_out_stage_5 (rg_finish == 0);
                       rg_finish <= 1;
                           let fpadder_4_out  <- fpadder_4.response.get();    
                           rg_out <= fpadder_4_out; 
                         //  $display("%b: rg_out",rg_out); 
                           $display("%b: fpadder_4_out",fpadder_4_out);  
                              
                  endrule 
                   
                   method Action ma_start (FloatingPoint#(4,3,6)  x, RoundMode rmode) if(rg_start == 0)   ;                  
                       rg_x <=  x; 
                       rg_rmode <= rmode; 
                       rg_start <= 1 ;                        
                   endmethod
           
                   method ActionValue#(FloatingPoint#(4,3,6)) mav_out() if(rg_finish == 1);
                      rg_start <= 0;   
                    return (rg_out);
                   endmethod 
endmodule 


 /*interface Ifc_activationfunc;      
       method Action ma_start (FloatingPoint#(4,3,6)  x , Bit#(3) func) ;
       method FloatingPoint#(4,3,6)  mv_output_value();        
   endinterface 
(*synthesize*)
module mkactivationfunc(Ifc_activationfunc); 

      Reg#(Bit#(1)) rg_valid <- mkRegA(0);
      Reg#(Bit#(3)) rg_func <- mkRegA(0);
      Reg#(FloatingPoint#(4,3,6)) rg_in  <- mkRegA(unpack(0));
      Reg#(FloatingPoint#(4,3,6)) rg_out  <- mkRegA(unpack(0));
      
      rule activfunc (rg_valid == 1); 
        rg_valid <= 0; 
          case(rg_func)  
               3'b000 :  if(rg_in.sign == true ) begin 
               
                FloatingPoint#(4,3,6) out_zero ;        
	          out_one.sign = False;
	          out_one.exp  = 4'b0000  ;
	          out_one.sfd  = 3'b000  ;
	          out_one.bias = 6'b000000 ; 
                   rg_out <= out_zero ; end 
                     else begin 
                         rg_out <= rg_in; end  
                         
              3'b001 :  if(rg_in.sign == true ) begin 
               
                FloatingPoint#(4,3,6) x ;        
	          out_one.sign = False;
	          out_one.exp  = 4'b0000  ;
	          out_one.sfd  = 3'b000  ;
	          out_one.bias = 6'b000000 + 8 ; 
                   rg_out <= out_zero ; end 
                     else begin 
                         rg_out <= rg_in; end
                         
               default :  
                            FloatingPoint#(4,3,6) out_zero ;        
	          out_one.sign = False;
	          out_one.exp  = 4'b0000  ;
	          out_one.sfd  = 3'b000  ;
	          out_one.bias = 6'b000000 ; 
                   rg_out <= out_zero ;
    
          
             endcase     
      
       endrule 
       

   method FloatingPoint#(4,3,6) val_data_in (FloatingPoint#(4,3,6)  x , Bit#(3) func);    
         rg_in <=  x; 
         rg_func <= func; 
         rg_valid <= 1;  
   endmethod 
   
   method FloatingPoint#(4,3,6)  mv_output_value();
        return(rg_out);
   endmethod
   */

endpackage
