/*
 * Generated by Bluespec Compiler (build 9f4a447)
 * 
 * On Thu Dec 14 09:13:00 IST 2023
 * 
 */

/* Generation options: */
#ifndef __model_mktb_float_h__
#define __model_mktb_float_h__

#include "bluesim_types.h"
#include "bs_module.h"
#include "bluesim_primitives.h"
#include "bs_vcd.h"

#include "bs_model.h"
#include "mktb_float.h"

/* Class declaration for a model of mktb_float */
class MODEL_mktb_float : public Model {
 
 /* Top-level module instance */
 private:
  MOD_mktb_float *mktb_float_instance;
 
 /* Handle to the simulation kernel */
 private:
  tSimStateHdl sim_hdl;
 
 /* Constructor */
 public:
  MODEL_mktb_float();
 
 /* Functions required by the kernel */
 public:
  void create_model(tSimStateHdl simHdl, bool master);
  void destroy_model();
  void reset_model(bool asserted);
  void get_version(unsigned int *year,
		   unsigned int *month,
		   char const **annotation,
		   char const **build);
  time_t get_creation_time();
  void * get_instance();
  void dump_state();
  void dump_VCD_defs();
  void dump_VCD(tVCDDumpType dt);
};

/* Function for creating a new model */
extern "C" {
  void * new_MODEL_mktb_float();
}

#endif /* ifndef __model_mktb_float_h__ */
