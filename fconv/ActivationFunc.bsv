package ActivationFunc;
import Floatingpoint::*;
import ClientServer::*;
import GetPut::*;
import FixedPoint :: * ;



 function  FixedPoint#(17,15) int_lut (Bit#(5) x);
    
      case(x)                                     
              5'b00000 : return unpack(unpack(32'b00000000000000001000000000000000)); //0
              5'b00001 : return unpack(unpack(32'b00000000000000010101101111110001)); //1
              5'b00010 : return unpack(unpack(32'b00000000000000111011000111001101)); //2
              5'b00011 : return unpack(unpack(32'b00000000000010100000101011110011)); //3 
              5'b00100 : return unpack(unpack(32'b00000000000110110100110010010000)); //4
              5'b00101 : return unpack(unpack(32'b00000000010010100011010011100010)); //5
              5'b00110 : return unpack(unpack(32'b00000000110010011011011011100011)); //6
              5'b00111 : return unpack(unpack(32'b00000010001001000101000100001011)); //7
              5'b01000 : return unpack(unpack(32'b00000101110100100111101010011111)); //8
              5'b01001 : return unpack(unpack(32'b00001111110100111000101010111110)); //9
              5'b01010 : return unpack(unpack(32'b00101011000001010011101110011111)); //10
              5'b01011 : return unpack(unpack(32'b01110100111100010001001000100100)); //11
              5'b01100 : return unpack(unpack(32'b01111111111111111111111111111111)); //12
              5'b01101 : return unpack(unpack(32'b01111111111111111111111111111111)); //13
              5'b01110 : return unpack(unpack(32'b01111111111111111111111111111111)); //14
              5'b01111 : return unpack(unpack(32'b01111111111111111111111111111111)); //15
              
              5'b10000 : return unpack(unpack(32'b00000000000000001000000000000000)); //-0
              5'b10001 : return unpack(unpack(32'b00000000000000000010111100010111)); //-1
              5'b10010 : return unpack(unpack(32'b00000000000000000001000101010011)); //-2
              5'b10011 : return unpack(unpack(32'b00000000000000000000011001011111)); //-3 
              5'b10100 : return unpack(unpack(32'b00000000000000000000001001011000)); //-4
              5'b10101 : return unpack(unpack(32'b00000000000000000000000011011101)); //-5
              5'b10110 : return unpack(unpack(32'b00000000000000000000000001010001)); //-6
              5'b10111 : return unpack(unpack(32'b00000000000000000000000000011110)); //-7
              5'b11000 : return unpack(unpack(32'b00000000000000000000000000001011)); //-8
              5'b11001 : return unpack(unpack(32'b00000000000000000000000000000100)); //-9
              5'b11010 : return unpack(unpack(32'b00000000000000000000000000000001)); //-10
              5'b11011 : return unpack(unpack(32'b00000000000000000000000000000001)); //-11
              5'b11100 : return unpack(unpack(32'b00000000000000000000000000000000)); //-12
              5'b11101 : return unpack(unpack(32'b00000000000000000000000000000000)); //-13
              5'b11110 : return unpack(unpack(32'b00000000000000000000000000000000)); //-14
              5'b11111 : return unpack(unpack(32'b00000000000000000000000000000000)); //-15
                        
                        
             endcase

 endfunction 

function  FixedPoint#(17,15) fac_lut (Bit#(5) x);
          
          case(x)  
               
              5'b00000 : return unpack(unpack(32'b00000000000000001000000000000000)); //0.0000
              5'b00001 : return unpack(unpack(32'b00000000000000001000100001000001)); //0.0625
              5'b00010 : return unpack(unpack(32'b00000000000000001001000100001011)); //0.1250
              5'b00011 : return unpack(unpack(32'b00000000000000001001101001100110)); //0.1875 
              5'b00100 : return unpack(unpack(32'b00000000000000001010010001011011)); //0.2500
              5'b00101 : return unpack(unpack(32'b00000000000000001010111011110101)); //0.3125
              5'b00110 : return unpack(unpack(32'b00000000000000001011101000111101)); //0.3750
              5'b00111 : return unpack(unpack(32'b00000000000000001100011001000000)); //0.4375
              5'b01000 : return unpack(unpack(32'b00000000000000001101001100001001)); //0.5000
              5'b01001 : return unpack(unpack(32'b00000000000000001110000010100110)); //0.5625
              5'b01010 : return unpack(unpack(32'b00000000000000001110111100100011)); //0.6250
              5'b01011 : return unpack(unpack(32'b00000000000000001111111010001111)); //0.6875
              5'b01100 : return unpack(unpack(32'b00000000000000010000111011111010)); //0.7500
              5'b01101 : return unpack(unpack(32'b00000000000000010010000001110100)); //0.8125
              5'b01110 : return unpack(unpack(32'b00000000000000010011001100001110)); //0.8750
              5'b01111 : return unpack(unpack(32'b00000000000000010100011011011100)); //0.9375 
                          
              5'b10000 : return unpack(unpack(32'b00000000000000001000000000000000)); //-0.0000
              5'b10001 : return unpack(unpack(32'b00000000000000000111100000111111)); //-0.0625
              5'b10010 : return unpack(unpack(32'b00000000000000000111000011110110)); //-0.1250
              5'b10011 : return unpack(unpack(32'b00000000000000000110101000011110)); //-0.1875 
              5'b10100 : return unpack(unpack(32'b00000000000000000110001110110000)); //-0.2500
              5'b10101 : return unpack(unpack(32'b00000000000000000101110110100110)); //-0.3125
              5'b10110 : return unpack(unpack(32'b00000000000000000101011111111001)); //-0.3750
              5'b10111 : return unpack(unpack(32'b00000000000000000101001010100101)); //-0.4375
              5'b11000 : return unpack(unpack(32'b00000000000000000100110110100011)); //-0.5000
              5'b11001 : return unpack(unpack(32'b00000000000000000100100011101111)); //-0.5625
              5'b11010 : return unpack(unpack(32'b00000000000000000100010010000011)); //-0.6250
              5'b11011 : return unpack(unpack(32'b00000000000000000100000001011101)); //-0.6875
              5'b11100 : return unpack(unpack(32'b00000000000000000011110001110111)); //-0.7500
              5'b11101 : return unpack(unpack(32'b00000000000000000011100011001101)); //-0.8125
              5'b11110 : return unpack(unpack(32'b00000000000000000011010101011100)); //-0.8750
              5'b11111 : return unpack(unpack(32'b00000000000000000011001000100000)); //-0.9375 
              
           
             endcase 
           

endfunction


 interface Ifc_FloatingPointExp;            
       method ActionValue#(FixedPoint#(17,15)) ma_start(FloatingPoint#(4,3,6)  x, RoundMode rmode, Bit#(3) ac_func ) ; 
     	      
   endinterface 
(*synthesize*)
module mkFloatingPointExp(Ifc_FloatingPointExp); 

   
      Reg#(Bit#(1)) rg_start  <- mkRegA(0);
      Reg#(Bit#(1)) rg_finish  <- mkRegA(0);

     
     function FixedPoint#(17,15)  fconv( Int#(8) exp , Bit#(4) sfd) ; 
        
          FixedPoint#(17,15) fix_val = unpack({ 16'b0,sfd,12'b0});
                 if (exp > 0) begin 
                     return (fix_val << exp);end
                   else   begin       
                       return ( fix_val >> (~exp + 1) );  end    
     endfunction 

                   
     
                  method ActionValue#(FixedPoint#(17,15)) ma_start(FloatingPoint#(4,3,6)  x, RoundMode rmode, Bit#(3) ac_func) ;  
             
                   
                       Int#(8) x_exp = zeroExtend(unpack(x.exp)) - zeroExtend(unpack(x.bias));  
                       Bit#(4) xsfd = { getHiddenBit(x), x.sfd };
                         $display("%d x.exp \n",x.exp);
                          $display("%d x.bias \n",x.bias);
                         $display("%b x_exp \n",x_exp);
                         $display("%b xsfd \n",xsfd);
            
                           Bit#(32) lv_res = pack(fconv (x_exp, xsfd));
                               $display("%b lv_res \n",lv_res); 
                           FixedPoint#(17,15) lv_res_fix =  unpack(lv_res);
                         
                           FixedPoint#(17,15) imprecise_part =   unpack(signExtend(~(lv_res[10:0] *(~(lv_res[10:0]>>1) * (~(lv_res[10:0] >>4  + lv_res[10:0] >> 2)))))) ; 
                           
                           FixedPoint#(17,15) precise_int_part = int_lut( { pack(x.sign),lv_res[18:15]}); 
                           FixedPoint#(17,15) precise_frac_part = fac_lut( { pack(x.sign),lv_res[14:11]});
                           
                             FixedPoint#(17,15) lv_exp = precise_int_part * (precise_frac_part + imprecise_part ) ;    
                             
                             FixedPoint#(17,15) activ_func = 0;  
                             
                             
                                case(ac_func)  
				       3'b000 : if (x.sign == True )  begin      //relu
				                     activ_func = 0; end 
				                     else begin 
				                     activ_func = lv_res_fix; end  
				        
				       3'b001 : if (x.sign == True )  begin                    //leaky relu
				                     activ_func = lv_res_fix >> 8 ; end 
				                     else begin 
				                     activ_func = lv_res_fix; end 
				                     
				       3'b010 : activ_func = (lv_exp / (lv_exp + 1)) ;     // sigmoid
				       
				       3'b011 :if (x.sign == True )  begin                    //Selu
				                     activ_func = lv_res_fix  ; end 
				                     else begin 
				                     activ_func = lv_exp -1 ; end 
				       
				       default : begin  let lv_exp2 = lv_exp*lv_exp; 
				           activ_func = ((lv_exp2 - 1) / (lv_exp2 + 1)) ; end  //tanh
				       
           
                                    endcase 
           
                                    
                           
                                        
                         
                          $display("%b imprecise_part \n",imprecise_part);  
                            fxptWrite(10,imprecise_part); 
                            $display("\n");
                          $display("%b lv_res_fix \n",lv_res_fix);  
                            fxptWrite(10,lv_res_fix);  
                           $display("\n");
                          $display("%b lv_exp \n",lv_exp); 
                           fxptWrite(10,lv_exp); 
                            $display("\n");
                           $display("%b activ_func \n",activ_func);  
                            fxptWrite(10,activ_func);
                           
                          return(activ_func); 
                         
                                                      
                   endmethod
           
endmodule 



endpackage
