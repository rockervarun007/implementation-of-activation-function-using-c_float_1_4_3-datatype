// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header,
// unless using verilator public meta comments.

#ifndef VERILATED_VMKTB_FLOAT__SYMS_H_
#define VERILATED_VMKTB_FLOAT__SYMS_H_  // guard

#include "verilated.h"

// INCLUDE MODEL CLASS

#include "Vmktb_float.h"

// INCLUDE MODULE CLASSES
#include "Vmktb_float___024root.h"

// SYMS CLASS (contains all model state)
class Vmktb_float__Syms final : public VerilatedSyms {
  public:
    // INTERNAL STATE
    Vmktb_float* const __Vm_modelp;
    bool __Vm_didInit = false;

    // MODULE INSTANCE STATE
    Vmktb_float___024root          TOP;

    // CONSTRUCTORS
    Vmktb_float__Syms(VerilatedContext* contextp, const char* namep, Vmktb_float* modelp);
    ~Vmktb_float__Syms();

    // METHODS
    const char* name() { return TOP.name(); }
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

#endif  // guard
