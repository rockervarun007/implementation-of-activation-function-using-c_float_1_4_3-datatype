// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vmktb_float.h for the primary calling header

#ifndef VERILATED_VMKTB_FLOAT___024ROOT_H_
#define VERILATED_VMKTB_FLOAT___024ROOT_H_  // guard

#include "verilated.h"

class Vmktb_float__Syms;

class Vmktb_float___024root final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(CLK,0,0);
    VL_IN8(RST_N,0,0);
    CData/*0:0*/ mktb_float__DOT__done_EN;
    CData/*1:0*/ mktb_float__DOT__rg_state;
    CData/*0:0*/ mktb_float__DOT__exp__DOT__rg_start;
    CData/*0:0*/ __Vtrigrprev__TOP__CLK;
    CData/*0:0*/ __Vtrigrprev__TOP__RST_N;
    CData/*0:0*/ __VactContinue;
    SData/*9:0*/ mktb_float__DOT__counter;
    SData/*9:0*/ mktb_float__DOT__counter_D_IN;
    IData/*31:0*/ __VstlIterCount;
    IData/*31:0*/ __VactIterCount;
    VlTriggerVec<1> __VstlTriggered;
    VlTriggerVec<3> __VactTriggered;
    VlTriggerVec<3> __VnbaTriggered;

    // INTERNAL VARIABLES
    Vmktb_float__Syms* const vlSymsp;

    // CONSTRUCTORS
    Vmktb_float___024root(Vmktb_float__Syms* symsp, const char* name);
    ~Vmktb_float___024root();
    VL_UNCOPYABLE(Vmktb_float___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
