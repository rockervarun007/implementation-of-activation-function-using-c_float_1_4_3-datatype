// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float___024root.h"

void Vmktb_float___024root___eval_act(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_act\n"); );
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__0(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__0\n"); );
    // Body
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("00000000000000010000000000000000 fixval\n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("%4#: Sent input ",10,vlSelf->mktb_float__DOT__counter);
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("<Float + 1. 0.0>");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("\n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY(vlSelf->mktb_float__DOT__done_EN)) {
            VL_WRITEF("00001000000000: Sent input \n");
            Verilated::runFlushCallbacks();
        }
    }
    if (vlSelf->RST_N) {
        if (VL_UNLIKELY((1U == (IData)(vlSelf->mktb_float__DOT__rg_state)))) {
            VL_FINISH_MT("verilog//mktb_float.v", 165, "");
        }
    }
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__1(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__1\n"); );
    // Body
    if ((1U & (~ (IData)(vlSelf->RST_N)))) {
        vlSelf->mktb_float__DOT__exp__DOT__rg_start = 0U;
    }
}

VL_INLINE_OPT void Vmktb_float___024root___nba_sequent__TOP__2(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_sequent__TOP__2\n"); );
    // Body
    if (vlSelf->RST_N) {
        vlSelf->mktb_float__DOT__counter = vlSelf->mktb_float__DOT__counter_D_IN;
        if (vlSelf->mktb_float__DOT__done_EN) {
            vlSelf->mktb_float__DOT__rg_state = 1U;
        }
    } else {
        vlSelf->mktb_float__DOT__counter = 0U;
        vlSelf->mktb_float__DOT__rg_state = 0U;
    }
    vlSelf->mktb_float__DOT__counter_D_IN = (0x3ffU 
                                             & ((IData)(1U) 
                                                + (IData)(vlSelf->mktb_float__DOT__counter)));
}

VL_INLINE_OPT void Vmktb_float___024root___nba_comb__TOP__0(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___nba_comb__TOP__0\n"); );
    // Body
    vlSelf->mktb_float__DOT__done_EN = ((~ (IData)(vlSelf->mktb_float__DOT__exp__DOT__rg_start)) 
                                        & (0U == (IData)(vlSelf->mktb_float__DOT__rg_state)));
}

void Vmktb_float___024root___eval_nba(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_nba\n"); );
    // Body
    if (vlSelf->__VnbaTriggered.at(1U)) {
        Vmktb_float___024root___nba_sequent__TOP__0(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(2U)) {
        Vmktb_float___024root___nba_sequent__TOP__1(vlSelf);
    }
    if (vlSelf->__VnbaTriggered.at(0U)) {
        Vmktb_float___024root___nba_sequent__TOP__2(vlSelf);
    }
    if ((vlSelf->__VnbaTriggered.at(0U) | vlSelf->__VnbaTriggered.at(2U))) {
        Vmktb_float___024root___nba_comb__TOP__0(vlSelf);
    }
}

void Vmktb_float___024root___eval_triggers__act(Vmktb_float___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__act(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG
#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__nba(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG

void Vmktb_float___024root___eval(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval\n"); );
    // Init
    VlTriggerVec<3> __VpreTriggered;
    IData/*31:0*/ __VnbaIterCount;
    CData/*0:0*/ __VnbaContinue;
    // Body
    __VnbaIterCount = 0U;
    __VnbaContinue = 1U;
    while (__VnbaContinue) {
        __VnbaContinue = 0U;
        vlSelf->__VnbaTriggered.clear();
        vlSelf->__VactIterCount = 0U;
        vlSelf->__VactContinue = 1U;
        while (vlSelf->__VactContinue) {
            vlSelf->__VactContinue = 0U;
            Vmktb_float___024root___eval_triggers__act(vlSelf);
            if (vlSelf->__VactTriggered.any()) {
                vlSelf->__VactContinue = 1U;
                if ((0x64U < vlSelf->__VactIterCount)) {
#ifdef VL_DEBUG
                    Vmktb_float___024root___dump_triggers__act(vlSelf);
#endif
                    VL_FATAL_MT("verilog//mktb_float.v", 29, "", "Active region did not converge.");
                }
                vlSelf->__VactIterCount = ((IData)(1U) 
                                           + vlSelf->__VactIterCount);
                __VpreTriggered.andNot(vlSelf->__VactTriggered, vlSelf->__VnbaTriggered);
                vlSelf->__VnbaTriggered.set(vlSelf->__VactTriggered);
                Vmktb_float___024root___eval_act(vlSelf);
            }
        }
        if (vlSelf->__VnbaTriggered.any()) {
            __VnbaContinue = 1U;
            if ((0x64U < __VnbaIterCount)) {
#ifdef VL_DEBUG
                Vmktb_float___024root___dump_triggers__nba(vlSelf);
#endif
                VL_FATAL_MT("verilog//mktb_float.v", 29, "", "NBA region did not converge.");
            }
            __VnbaIterCount = ((IData)(1U) + __VnbaIterCount);
            Vmktb_float___024root___eval_nba(vlSelf);
        }
    }
}

#ifdef VL_DEBUG
void Vmktb_float___024root___eval_debug_assertions(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->CLK & 0xfeU))) {
        Verilated::overWidthError("CLK");}
    if (VL_UNLIKELY((vlSelf->RST_N & 0xfeU))) {
        Verilated::overWidthError("RST_N");}
}
#endif  // VL_DEBUG
