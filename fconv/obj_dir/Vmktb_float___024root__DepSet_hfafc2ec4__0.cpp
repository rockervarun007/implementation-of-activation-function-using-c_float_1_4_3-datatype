// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float__Syms.h"
#include "Vmktb_float___024root.h"

#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__act(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG

void Vmktb_float___024root___eval_triggers__act(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_triggers__act\n"); );
    // Body
    vlSelf->__VactTriggered.at(0U) = ((IData)(vlSelf->CLK) 
                                      & (~ (IData)(vlSelf->__Vtrigrprev__TOP__CLK)));
    vlSelf->__VactTriggered.at(1U) = ((~ (IData)(vlSelf->CLK)) 
                                      & (IData)(vlSelf->__Vtrigrprev__TOP__CLK));
    vlSelf->__VactTriggered.at(2U) = (((IData)(vlSelf->CLK) 
                                       & (~ (IData)(vlSelf->__Vtrigrprev__TOP__CLK))) 
                                      | ((~ (IData)(vlSelf->RST_N)) 
                                         & (IData)(vlSelf->__Vtrigrprev__TOP__RST_N)));
    vlSelf->__Vtrigrprev__TOP__CLK = vlSelf->CLK;
    vlSelf->__Vtrigrprev__TOP__RST_N = vlSelf->RST_N;
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vmktb_float___024root___dump_triggers__act(vlSelf);
    }
#endif
}
