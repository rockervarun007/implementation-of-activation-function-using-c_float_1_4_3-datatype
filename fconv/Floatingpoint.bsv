////////////////////////////////////////////////////////////////////////////////
//  Filename      : FloatingPoint.bsv
//  Description   : General purpose floating point library
////////////////////////////////////////////////////////////////////////////////
package Floatingpoint;

// Notes :
// increasing exp = right shift sfd
// decreasing exp = left shift sfd

////////////////////////////////////////////////////////////////////////////////
/// Imports
////////////////////////////////////////////////////////////////////////////////
import Real              ::*;
import Vector            ::*;
import BUtils            ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FIFOF        ::*;
import FixedPoint        ::*;
import Divide ::*;
import DefaultValue ::*;

////////////////////////////////////////////////////////////////////////////////
/// Exports
////////////////////////////////////////////////////////////////////////////////
export FloatingPoint(..);
export RoundMode(..);
export isSubNormal;
export isZero;
export mkFloatingPointAdder;
export mkFloatingPointMultiplier;
export mkFloatingPointDivider;
export mkFloatingPointDividerCfloat143;
export zero;
export getHiddenBit;
export Cfloat_1_4_3;
export addFP;
export multFP;

////////////////////////////////////////////////////////////////////////////////
/// Types
////////////////////////////////////////////////////////////////////////////////

typedef struct {
   Bool        sign;
   Bit#(e)     exp;
   Bit#(m)     sfd;
   Bit#(b)     bias;
} FloatingPoint#(numeric type e, numeric type m, numeric type b) deriving (Bits);

instance DefaultValue#( FloatingPoint#(e,m,b) );
   defaultValue = FloatingPoint {
      sign:       False,
      exp:        0,
      sfd:        0,
      bias:       0
      };
endinstance

typedef enum {
   LT,
   EQ,
   GT,
   UO
   } Disorder deriving (Bits, Eq, Bounded);


instance FShow#( FloatingPoint#(e,m,b) );
   function Fmt fshow( FloatingPoint#(e,m,b) value );
      return $format("<Float %s%d.%d.%d>", value.sign ? "-" : "+", value.exp,value.bias, value.sfd);
   endfunction
endinstance

typedef enum {
     Rnd_Nearest_Even
   , Rnd_Nearest_Away_Zero
   , Rnd_Plus_Inf
   , Rnd_Minus_Inf
   , Rnd_Zero
} RoundMode deriving (Bits, Eq);

instance DefaultValue#(RoundMode);
   defaultValue = Rnd_Nearest_Even;
endinstance

instance FShow#( RoundMode );
   function Fmt fshow( RoundMode value );
      case(value)
	 Rnd_Nearest_Even:      return $format("<Round Mode: Nearest Even>");
	 Rnd_Nearest_Away_Zero: return $format("<Round Mode: Nearest Away From Zero>");
	 Rnd_Plus_Inf:          return $format("<Round Mode: +Infinity>");
	 Rnd_Minus_Inf:         return $format("<Round Mode: -Infinity>");
	 Rnd_Zero:              return $format("<Round Mode: Zero>");
      endcase
   endfunction
endinstance

////////////////////////////////////////////////////////////////////////////////
/// Float Formats
////////////////////////////////////////////////////////////////////////////////
typedef FloatingPoint#(4,3,6) Cfloat_1_4_3;
////////////////////////////////////////////////////////////////////////////////
/// Functions
////////////////////////////////////////////////////////////////////////////////
// Zero extend a quantity by padding on the LSB side.
function Bit#(m) zExtendLSB(Bit#(n) value)
   provisos( Add#(n,m,k) );
   Bit#(k) out = { value, 0 };
   return out[valueof(k)-1:valueof(n)];
endfunction

// Zero extend and change type by padding on the LSB side (of bits instance)
function a cExtendLSB(b value)
   provisos( Bits#(a,sa), Bits#(b,sb) );
   let out = unpack(zExtendLSB(pack(value)));
   return out;
endfunction

// Returns the 1-based index (or 0 if not found) of the first 1
// from the MSB down.
function Integer findIndexOneMSB_( Bit#(s) din );
   Vector#(s, Bit#(1)) v = unpack(din);
   Integer result = 0;
   for(Integer i = 0; i < valueof(s); i = i + 1) begin
      if (v[i] == 1) result = i + 1;
   end
   return result;
endfunction

function UInt#(l) findIndexOneMSB( Bit#(s) din )
   provisos( Add#(_, 1, s), Log#(s, logs), Add#(logs,1,l));
   Vector#(s, Bit#(1)) v = unpack(reverseBits(din));
   if (findElem(1'b1, v) matches tagged Valid .ridx) begin
      return fromInteger(valueOf(s)) - cExtend(ridx);
   end
   else begin
      return 0;
   end
endfunction

// Returns the 1-based index (or 0 if not found) of the first 1
// from the LSB up.
function Integer findIndexOneLSB_( Bit#(s) din );
   Vector#(s, Bit#(1)) v = unpack(din);
   Integer result = 0;
   Bool done = False;
   for(Integer i = 0; i < valueof(s); i = i + 1) begin
      if (v[i] == 1)  done = True;
      else if (!done) result = i + 1;
   end
   return (done) ? result : 0;
endfunction

function UInt#(l) findIndexOneLSB( Bit#(s) din )
   provisos( Add#(_, 1, s), Log#(s, logs), Add#(logs,1,l));
   Vector#(s, Bit#(1)) v = unpack(din);
   if (findElem(1'b1, v) matches tagged Valid .ridx) begin
      return cExtend(ridx);
   end
   else begin
      return 0;
   end
endfunction

function FloatingPoint#(e,m,b) zero(Bool sign);
   return FloatingPoint {
      sign:     sign,
      exp:      0,
      sfd:      0,
      bias:     0
      };
endfunction

function Bool isZero( FloatingPoint#(e,m,b) din );
   return isSubNormal(din) && (din.sfd == 0);
endfunction

function Bool isOne( FloatingPoint#(e,m,b) din );
   return (din.sfd == 0) && (din.exp == fromInteger(bias(din)));
endfunction

function Bool isNegativeZero( FloatingPoint#(e,m,b) din );
   return isZero(din) && (din.sign);
endfunction

function Bool isSubNormal( FloatingPoint#(e,m,b) din );
   return (din.exp == 0);
endfunction

function Integer bias( FloatingPoint#(e,m,b) din );
   return (2 ** (valueof(e)-1)) - 1;
endfunction

function Bit#(e) unbias( FloatingPoint#(e,m,b) din );
   return (din.exp - fromInteger(bias(din)));
endfunction

function Bit#(1) getHiddenBit( FloatingPoint#(e,m,b) din );
   return (isSubNormal(din)) ? 0 : 1;
endfunction

function Integer minexp( FloatingPoint#(e,m,b) din );
  return 2 - (2 ** valueof(b) );
endfunction

function Integer minexp_subnormal( FloatingPoint#(e,m,b) din );
   return minexp(din)-valueof(m);
endfunction

function Integer maxexp( FloatingPoint#(e,m,b) din );
   return (2 ** valueof(e)) - 1;
endfunction

function FloatingPoint#(e,m,b) rightshift( FloatingPoint#(e,m,b) din, Bit#(e) amt )
   provisos(  Add#(m, 4, m4)
	    , Add#(m4, m, x)
	    );
   Bit#(x) sfd = cExtendLSB({ getHiddenBit(din), din.sfd });
   Bit#(1) hidden;
   Bit#(m) s;
   Bit#(m) rest;
   { hidden, s, rest } = cExtendLSB(sfd >> amt);
   din.sfd    = s;
   return din;
endfunction

function FloatingPoint#(e,m,b) round( RoundMode rmode, FloatingPoint#(e,m,b) din, Bit#(2) guard )
   provisos(  Add#(m, 1, m1)
	    , Add#(m, 2, m2)
	    );

   FloatingPoint#(e,m,b) out = defaultValue;

      let din_inc = din;

      Bit#(TAdd#(m,2)) sfd = unpack({1'b0, getHiddenBit(din), din.sfd}) + 1;

      if (msb(sfd) == 1) begin
	      if (din.exp == '1) begin
            if ( din.bias==0)
	            din_inc.sfd = '1;
            else begin
               din_inc.bias = din_inc.bias - 1;
               din_inc.sfd = truncate(sfd >> 1);
            end
	      end
	      else begin
	         din_inc.exp = din_inc.exp + 1;
	         din_inc.sfd = truncate(sfd >> 1);
	      end
      end
      else if ((din.exp == 0) && (truncateLSB(sfd) == 2'b01)) begin
	      din_inc.exp = 1;
	      din_inc.sfd = truncate(sfd);
      end
      else begin
	      din_inc.sfd = truncate(sfd);
      end

      case(rmode)
	 Rnd_Nearest_Even:
	 begin
	    case (guard)
	       'b00: out = din;
	       'b01: out = din;
	       'b10: out = (lsb(din.sfd) == 1) ? din_inc : din;
	       'b11: out = din_inc;
	    endcase
	 end

      endcase

   return out;
endfunction

// Function for 3 bit mantissa
function FloatingPoint#(e,m,b) normalize_subnormal(FloatingPoint#(e,m,b) din)
   provisos(
   );
   FloatingPoint#(e,m,b) out = din;
   if ((din.exp == 0) && (din.bias!='1) && (din.sfd!=0) ) begin
      if (din.bias < '1 - 3) begin
         //normal
         if (din.sfd[2] == 1) begin
            out.sfd = din.sfd << 1;
            out.bias = din.bias + 2;
         end
         else if (din.sfd[1] == 1) begin
            //normal
            out.sfd = din.sfd << 2;
            out.bias = din.bias + 3;
         end
         else begin
            //normal
            out.sfd = 0;
            out.bias = din.bias + 4;
         end
         din.exp = 1;
      end
      else if (din.bias == '1 - 3) begin
         if (din.sfd[2] == 1) begin
            //normal
            out.sfd = din.sfd << 1;
            out.bias = din.bias + 2;
            din.exp = 1;
         end
         else if (din.sfd[1] == 1) begin
            //normal
            out.sfd = din.sfd << 2;
            out.bias = din.bias + 3;
            din.exp = 1;
         end
         else begin
            //subnormal
            out.sfd = din.sfd << 2;
            out.bias = din.bias + 3;
         end
         
      end
      else if (din.bias == '1 - 2) begin
         if (din.sfd[2] == 1) begin
            //normal
            out.sfd = din.sfd << 1;
            out.bias = din.bias + 2;
            din.exp = 1;
         end
	//  $display("guard = 'b%b", guard);
         else if (din.sfd[1] == 1) begin
            out.sfd = din.sfd << 1;
            out.bias = din.bias + 1;
         end
         else begin
            out.sfd = din.sfd << 2;
            out.bias = din.bias + 2;
         end
      end
      else if (din.bias == '1 - 1) begin
         if (din.sfd[2] != 1) begin
            out.sfd = din.sfd << 1;
            out.bias = din.bias + 1;
         end
      end
   end
   return out;


endfunction

function Tuple2#(FloatingPoint#(e,m,b),Bit#(2)) normalize( FloatingPoint#(e,m,b) din, Bit#(x) sfdin )
   provisos(
      Add#(1, a__, x),
      Add#(m, b__, x),
      // per request of bsc
      Add#(c__, TLog#(TAdd#(1, x)), TAdd#(e, 1)),
      Add#(d__, e, TAdd#(b, 1)),
      Add#(e__, TLog#(TAdd#(1, x)), TAdd#(b, 1))
      );

   FloatingPoint#(e,m,b) out = din;
   Bit#(2) guard = 0;
   Int#(TAdd#(b,1)) exp = zeroExtend(unpack(out.exp)) - zeroExtend(unpack(out.bias));
   let zeros = countZerosMSB(sfdin);

   if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) begin
      out.exp = '1;
      out.sfd = '1;
      guard = '1;
   end
   else begin
      if (zeros == 0) begin
	      // carry, no sfd adjust necessary

	      if (out.exp == 0)
	         out.exp = 2;
	      else
	         out.exp = out.exp + 1;

	      // carry bit
	      sfdin = sfdin << 1;
      end
      else if (zeros == 1) begin
	      // already normalized

	      if (out.exp == 0)
	         out.exp = 1;

	      // carry, hidden bits
	      sfdin = sfdin << 2;
      end
      else if (zeros == fromInteger(valueOf(x))) begin
	      // exactly zero
	      out.exp = 0;
      end
      else begin
         // try to normalize
         Int#(TAdd#(b,1)) shift = zeroExtend(unpack(pack(zeros - 1)));
         Int#(TAdd#(b,1)) maxshift = exp - fromInteger(minexp(out));

         if (shift > maxshift) begin
            // result will be subnormal

            sfdin = sfdin << maxshift;
            out.exp = 0;
            out.bias = '1;
         end
         else begin
            // result will be normal

            sfdin = sfdin << shift;
            out.bias = out.bias + truncate(pack(shift));
         end

 	      // carry, hidden bits
	      sfdin = sfdin << 2;
      end

      out.sfd = unpack(truncateLSB(sfdin));
      sfdin = sfdin << fromInteger(valueOf(m));

      guard[1] = unpack(truncateLSB(sfdin));
      sfdin = sfdin << 1;

      guard[0] = |sfdin;
   end

   return tuple2(out,guard);
endfunction

////////////////////////////////////////////////////////////////////////////////
/// Addition/Subtraction
////////////////////////////////////////////////////////////////////////////////
function FloatingPoint#(e,m,b) addFP ( FloatingPoint#(e,m,b) in1, FloatingPoint#(e,m,b) in2, RoundMode rmode )
   provisos(
      // per request of bsc
      Add#(a__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(e, 1)),
      Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(b, 1)),
      Add#(c__, e, TAdd#(b, 1)),
      Add#(d__, e, TAdd#(b, 2))
      );

   function Tuple8#(CommonState#(e,m,b),
		    Bit#(TAdd#(m,5)),
		    Bit#(TAdd#(m,5)),
		    Bool,
		    Bool,
		    Bit#(e),
		    Bit#(b),
		    Bit#(b)) s1_stage(Tuple3#(FloatingPoint#(e,m,b),
					      FloatingPoint#(e,m,b),
					      RoundMode) op);

      match { .opA, .opB, .rmode } = op;

      CommonState#(e,m,b) s = CommonState {
	 res: tagged Invalid,
	 rmode: rmode
	 };

      Int#(TAdd#(b,2)) expA = zeroExtend(unpack(opA.exp)) - zeroExtend(unpack(opA.bias));
      Int#(TAdd#(b,2)) expB = zeroExtend(unpack(opB.exp)) - zeroExtend(unpack(opB.bias));

      Bit#(TAdd#(m,5)) sfdA = {1'b0, getHiddenBit(opA), opA.sfd, 3'b0};
      Bit#(TAdd#(m,5)) sfdB = {1'b0, getHiddenBit(opB), opB.sfd, 3'b0};

      Bit#(TAdd#(m,5)) x;
      Bit#(TAdd#(m,5)) y;
      Bool sgn;
      Bool sub;
      Bit#(e) exp;
      Bit#(b) bias;
      Bit#(b) expdiff;

       if ((expB > expA) || ((expB == expA) && (sfdB > sfdA))) begin
	 exp = opB.exp;
	 expdiff = truncate(pack(expB - expA));
         bias = opB.bias;
	 x = sfdB;
	 y = sfdA;
	 sgn = opB.sign;
	 sub = (opB.sign != opA.sign);
      end
      else begin
	 exp = opA.exp;
	 expdiff = truncate(pack(expA - expB));
         bias = opA.bias;
	 x = sfdA;
	 y = sfdB;
	 sgn = opA.sign;
	 sub = (opA.sign != opB.sign);
      end


      return tuple8(s,
			   x,
			   y,
			   sgn,
			   sub,
			   exp,
            bias,
			   expdiff);
   endfunction

   function Tuple7#(CommonState#(e,m,b),
		    Bit#(TAdd#(m,5)),
		    Bit#(TAdd#(m,5)),
		    Bool,
		    Bool,
		    Bit#(e),
		    Bit#(b)) s2_stage(Tuple8#(CommonState#(e,m,b),
					      Bit#(TAdd#(m,5)),
					      Bit#(TAdd#(m,5)),
					      Bool,
					      Bool,
					      Bit#(e),
		                              Bit#(b),
		                              Bit#(b)) op);

      match {.s, .opA, .opB, .sign, .subtract, .exp, .bias, .diff} = op;

      if (s.res matches tagged Invalid) begin
	 if (diff < fromInteger(valueOf(m) + 5)) begin
	    Bit#(TAdd#(m,5)) guard = opB;

	    guard = opB << (fromInteger(valueOf(m) + 5) - diff);
	    opB = opB >> diff;
	    opB[0] = opB[0] | (|guard);
	 end
	 else if (|opB == 1) begin
	    opB = 1;
	 end
      end

      return tuple7(s,
		    opA,
		    opB,
		    sign,
		    subtract,
		    exp,
		    bias);
   endfunction

   function Tuple7#(CommonState#(e,m,b),
		    Bit#(TAdd#(m,5)),
		    Bit#(TAdd#(m,5)),
		    Bool,
		    Bool,
		    Bit#(e),
		    Bit#(b)) s3_stage(Tuple7#(CommonState#(e,m,b),
					      Bit#(TAdd#(m,5)),
					      Bit#(TAdd#(m,5)),
					      Bool,
					      Bool,
					      Bit#(e),
					      Bit#(b)) op);

      match {.s, .a, .b, .sign, .subtract, .exp, .bias} = op;

      let sum = a + b;
      let diff = a - b;

      return tuple7(s,
		    sum,
		    diff,
		    sign,
		    subtract,
		    exp,
                   bias);
   endfunction

   function  FloatingPoint#(e,m,b) s4_stage(Tuple7#(CommonState#(e,m,b),
					   Bit#(TAdd#(m,5)),
					   Bit#(TAdd#(m,5)),
					   Bool,
					   Bool,
					   Bit#(e),
					   Bit#(b)) op);

      match {.s, .addres, .subres, .sign, .subtract, .exp, .bias} = op;

      FloatingPoint#(e,m,b) out = defaultValue;
      Bit#(2) guard = 0;

      if (s.res matches tagged Invalid) begin
	 Bit#(TAdd#(m,5)) result;

	 if (subtract) begin
	    result = subres;
	 end
	 else begin
            result = addres;
	 end

	 out.sign = sign;
	 out.exp = exp;
	 out.bias = bias;

	 // $display("out = ", fshow(out));
	 // $display("result = 'h%x", result);
	 // $display("zeros = %d", countZerosMSB(result));

	 let y = normalize(out, result);
	 out = tpl_1(y);
	 guard = tpl_2(y);
	// s.exc = s.exc | tpl_3(y);
      end

      /*return tuple4(s,
		    out,
		    guard,
		    subtract);*/ 
		    
		return(out);    
   endfunction

   /*function Tuple2#(FloatingPoint#(e,m,b),
		    Exception) s5_stage(Tuple4#(CommonState#(e,m,b),
						FloatingPoint#(e,m,b),
						Bit#(2),
						Bool) op);

      match {.s, .rnd, .guard, .subtract} = op;

      FloatingPoint#(e,m,b) out = rnd;

      if (s.res matches tagged Valid .x) begin
	 out = x;
      end
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);
      end

      // adjust sign for exact zero result
      if (isZero(out) && !s.exc.inexact && subtract) begin
	 out.sign = (s.rmode == Rnd_Minus_Inf);
      end

      return tuple2(out,s.exc);
   endfunction*/

   return /*s5_stage(*/ s4_stage( s3_stage( s2_stage( s1_stage(tuple3(in1,in2,rmode)) ) ) ) /*)*/;
endfunction

////////////////////////////////////////////////////////////////////////////////
/// Multiply
////////////////////////////////////////////////////////////////////////////////
function FloatingPoint#(e,m,b) multFP ( FloatingPoint#(e,m,b) in1, FloatingPoint#(e,m,b) in2, RoundMode rmode )
   provisos(
      // per request of bsc
      Add#(a__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(e, 1)),
      Add#(e__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(b, 1)),
      Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(b, 1)),
      Add#(c__, e, TAdd#(b, 1)),
      Add#(d__, e, TAdd#(b, 2))
      );

   function Tuple5#(CommonState#(e,m,b),
		    Bit#(TAdd#(m,1)),
		    Bit#(TAdd#(m,1)),
		    Int#(TAdd#(b,2)),
		    Bool) s1_stage(Tuple3#(FloatingPoint#(e,m,b),
					   FloatingPoint#(e,m,b),
					   RoundMode) op);

    match { .opA, .opB, .rmode } = op;

      CommonState#(e,m,b) s = CommonState {
	 res: tagged Invalid,
	 rmode: rmode
	 };

    Int#(TAdd#(b,2)) expA = zeroExtend(unpack(opA.exp)) - zeroExtend(unpack(opA.bias));
    Int#(TAdd#(b,2)) expB = zeroExtend(unpack(opB.exp)) - zeroExtend(unpack(opB.bias));
      Int#(TAdd#(b,2)) newexp = expA + expB;

      Bool sign = (opA.sign != opB.sign);

      Bit#(TAdd#(m,1)) opAsfd = { getHiddenBit(opA), opA.sfd };
      Bit#(TAdd#(m,1)) opBsfd = { getHiddenBit(opB), opB.sfd };

   if (isZero(opA) || isZero(opB)) begin
	 s.res = tagged Valid zero(opA.sign != opB.sign);
   end
   else if (newexp > fromInteger(maxexp(opA))) begin
	 FloatingPoint#(e,m,b) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = '1;
	 out.sfd = '1;
    out.bias = 0;

	//  s.exc.overflow = True;
	//  s.exc.inexact = True;

	//  let y = round(rmode, out, '1);
	 s.res = tagged Valid out;
      end
   else if (newexp < (fromInteger(minexp_subnormal(opA))-2)) begin
	 FloatingPoint#(e,m,b) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = 0;
	 out.sfd = 0;
    out.bias = 0;

	//  s.exc.underflow = True;
	//  s.exc.inexact = True;

	//  let y = round(rmode, out, 'b01);
	 s.res = tagged Valid out;
	//  s.exc = s.exc | tpl_2(y);
      end

      return tuple5(s,
		    opAsfd,
		    opBsfd,
		    newexp,
		    sign);
   endfunction

   function Tuple4#(CommonState#(e,m,b),
		    Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		    Int#(TAdd#(b,2)),
		    Bool) s2_stage(Tuple5#(CommonState#(e,m,b),
					   Bit#(TAdd#(m,1)),
					   Bit#(TAdd#(m,1)),
					   Int#(TAdd#(b,2)),
					   Bool) op);

      match {.s, .opAsfd, .opBsfd, .exp, .sign} = op;

      Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))) sfdres = primMul(opAsfd, opBsfd);

      return tuple4(s,
		    sfdres,
		    exp,
		    sign);
   endfunction

   function Tuple4#(CommonState#(e,m,b),
		 Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		 Int#(TAdd#(b,2)),
		 Bool) s3_stage(Tuple4#(CommonState#(e,m,b),
		                        Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		                        Int#(TAdd#(b,2)),
		                        Bool) op);
      return op;
   endfunction

   function  FloatingPoint#(e,m,b) s4_stage(Tuple4#(CommonState#(e,m,b),
		                              Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		                              Int#(TAdd#(b,2)),
		                              Bool) op);

       match {.s, .sfdres, .exp, .sign} = op;

      FloatingPoint#(e,m,b) result = defaultValue;
      Bit#(2) guard = ?;

      if (s.res matches tagged Invalid) begin
	 //$display("sfdres = 'h%x", sfdres);

	 let shift = fromInteger(minexp(result)) - exp;
    if (shift > 1) begin
	   // subnormal
	   Bit#(1) sfdlsb = |(sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift));

	   //$display("sfdlsb = |'h%x = 'b%b", (sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift)), sfdlsb);

      sfdres = sfdres >> (shift-1);
      sfdres[0] = sfdres[0] | sfdlsb;

	   result.exp = 0;
      result.bias = '1;
	 end
	 else if (shift == 1) begin
	   // subnormal
	   Bit#(1) sfdlsb = |(sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift));

	   //$display("sfdlsb = |'h%x = 'b%b", (sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift)), sfdlsb);

      sfdres = sfdres >> 1;
      sfdres[0] = sfdres[0] | sfdlsb;

	   result.exp = 0;
      result.bias = '1-1;
	 end
	 else if (exp > 0) begin
      result.exp = truncate(pack(exp));
      result.bias = 0;
    end
    else begin
      result.exp = 1;
	   result.bias = cExtend(-exp + 1);
	 end
    result.sign = sign;

	//  $display("mul shift = %d", shift);
	//  $display("mul sfdres = 'h%x", sfdres);
	//  $display("mul result = ", fshow(result));
	//  $display("mul zeros = %d", countZerosMSB(sfdres));
	 
	 let y = normalize(result, sfdres);
	 result = tpl_1(y);
	 guard = tpl_2(y);
	//  s.exc = s.exc | tpl_3(y);

	//  $display("mul result = ", fshow(result));
	 // $display("exc = 'b%b", pack(exc));
      end

      /*return tuple3(s,
		    result,
		    guard);*/
		    
		return(result);     
   endfunction

   /*function Tuple2#(FloatingPoint#(e,m),
		    Exception) s5_stage(Tuple3#(CommonState#(e,m),
						FloatingPoint#(e,m),
						Bit#(2)) op);

      match {.s, .rnd, .guard} = op;

      FloatingPoint#(e,m) out = rnd;

      if (s.res matches tagged Valid .x)
	 out = x;
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);
      end

      return tuple2(out,s.exc);
   endfunction*/

   return /*s5_stage(*/ s4_stage( s3_stage( s2_stage( s1_stage(tuple3(in1,in2,rmode)) ) ) ) /*)*/;
endfunction

////////////////////////////////////////////////////////////////////////////////
/// Divide
////////////////////////////////////////////////////////////////////////////////
/*function Tuple2#(FloatingPoint#(e,m),Exception) divFP ( FloatingPoint#(e,m) in1, FloatingPoint#(e,m) in2, RoundMode rmode )
   provisos(
      Add#(e,1,ebits),
      Add#(ebits,1,ebits1),
      Add#(m,1,m1bits),
      Add#(m,5,dbits),
      Add#(dbits,1,dbits1),
      Add#(dbits,dbits,nbits),
      // per request of bsc
      Add#(a__, TLog#(TAdd#(1, dbits1)), TAdd#(e, 1)),
      Add#(m, b__, dbits1),
      Add#(2, c__, dbits1),
      Add#(d__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 5), 1))), TAdd#(e, 1)),
      Add#(e__, e, ebits1),
      Add#(f__, TLog#(TAdd#(1, m1bits)), ebits1)
     );

   function Tuple7#(Maybe#(FloatingPoint#(e,m)),
		    Exception,
		    RoundMode,
		    FloatingPoint#(e,m),
		    Bit#(nbits),
		    Bit#(dbits),
		    Bit#(e)) s1_stage(Tuple3#(FloatingPoint#(e,m),
					      FloatingPoint#(e,m),
					      RoundMode) op);

      match {.in1, .in2, .rmode} = op;
      Maybe#(FloatingPoint#(e,m)) out = tagged Invalid;
      Exception exc = defaultValue;
      FloatingPoint#(e,m) result = defaultValue;
      Bit#(m1bits) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(m1bits) sfdB = {getHiddenBit(in2), in2.sfd};
      Bit#(e) shift = 0;

      let zerosA = countZerosMSB(sfdA);
      sfdA = sfdA << zerosA;

      let zerosB = countZerosMSB(sfdB);
      sfdB = sfdB << zerosB;

      // calculate the new exponent
      Int#(ebits1) exp1 = isSubNormal(in1) ? fromInteger(minexp(in1)) : signExtend(unpack(unbias(in1)));
      Int#(ebits1) exp2 = isSubNormal(in2) ? fromInteger(minexp(in2)) : signExtend(unpack(unbias(in2)));
      Int#(ebits1) newexp = (exp1 - zeroExtend(unpack(pack(zerosA)))) - (exp2 - zeroExtend(unpack(pack(zerosB))));

      Bit#(nbits) opA = zExtendLSB({ 1'b0, sfdA });
      Bit#(dbits) opB = zExtend({ sfdB, 4'b0000 });

      // calculate the sign
      result.sign = in1.sign != in2.sign;

      if (isSNaN(in1)) begin
	 out = tagged Valid nanQuiet(in1);
	 exc.invalid_op = True;
      end
      else if (isSNaN(in2)) begin
	 out = tagged Valid nanQuiet(in2);
	 exc.invalid_op = True;
      end
      else if (isQNaN(in1)) begin
	 out = tagged Valid in1;
      end
      else if (isQNaN(in2)) begin
	 out = tagged Valid in2;
      end
      else if ((isInfinity(in1) && isInfinity(in2)) || (isZero(in1) && isZero(in2))) begin
	 out = tagged Valid qnan();
	 exc.invalid_op = True;
      end
      else if (isZero(in2) && !isInfinity(in1)) begin
	 out = tagged Valid infinity(result.sign);
	 exc.divide_0 = True;
      end
      else if (isInfinity(in1)) begin
	 out = tagged Valid infinity(result.sign);
      end
      else if (isZero(in1) || isInfinity(in2)) begin
	 out = tagged Valid zero(result.sign);
      end
      else if (newexp > fromInteger(maxexp(in1)+1)) begin
	 result.exp = maxBound - 1;
	 result.sfd = maxBound;

	 exc.overflow = True;
	 exc.inexact = True;

	 let y = round(rmode, result, '1);
	 out = tagged Valid tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if (newexp < (fromInteger(minexp_subnormal(in1))-2)) begin
	 result.exp = 0;
	 result.sfd = 0;

	 exc.underflow = True;
	 exc.inexact = True;

	 let y = round(rmode, result, 'b01);
	 out = tagged Valid tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if (newexp < fromInteger(minexp(result))) begin
	 result.exp = 0;
	 shift = cExtend(fromInteger(minexp(result)) - newexp);
      end
      else begin
	 result.exp = cExtend(newexp + fromInteger(bias(result)));
      end

      return tuple7(out,exc,rmode,result,opA,opB,shift);
   endfunction

   function Tuple5#(Maybe#(FloatingPoint#(e,m)),
		    Exception,
		    RoundMode,
		    FloatingPoint#(e,m),
                    Bit#(dbits1)) s3_stage(Tuple7#(Maybe#(FloatingPoint#(e,m)),
						   Exception,
						   RoundMode,
						   FloatingPoint#(e,m),
						   Bit#(nbits),
						   Bit#(dbits),
						   Bit#(e)) op);

      match {.out,.exc,.rmode,.result,.opA,.opB,.shift} = op;

      Bit#(dbits1) rsfd = ?;

      if (out matches tagged Invalid) begin
	 UInt#(dbits) q = truncate(unpack(opA) / extend(unpack(opB)));
	 UInt#(dbits) p = truncate(unpack(opA) % extend(unpack(opB)));

	 if (shift < fromInteger(valueOf(dbits1))) begin
	    Bit#(1) sfdlsb = |(pack(q << (fromInteger(valueOf(dbits1)) - shift)));
	    rsfd = cExtend(q >> shift);
	    rsfd[0] = rsfd[0] | sfdlsb;
	 end
	 else begin
	    Bit#(1) sfdlsb = |(pack(q));
	    rsfd = 0;
	    rsfd[0] = sfdlsb;
	 end

	 if (p != 0) begin
	    rsfd[0] = 1;
	 end

	 //$display(" = %d, %d", q, p);
      end

      return tuple5(out,exc,rmode,result,rsfd);
   endfunction

   function Tuple5#(Maybe#(FloatingPoint#(e,m)),
                    Exception,
                    RoundMode,
		    FloatingPoint#(e,m),
		    Bit#(2)) s4_stage(Tuple5#(Maybe#(FloatingPoint#(e,m)),
		                              Exception,
		                              RoundMode,
		                              FloatingPoint#(e,m),
                                              Bit#(dbits1)) op);

      match {.out,.exc,.rmode,.result,.rsfd} = op;

      Bit#(2) guard = ?;

      if (result.exp == maxBound) begin
	 if (truncateLSB(rsfd) == 2'b00) begin
	    rsfd = rsfd << 1;
	    result.exp = result.exp - 1;
	 end
	 else begin
	    result.exp = maxBound - 1;
	    result.sfd = maxBound;

	    exc.overflow = True;
	    exc.inexact = True;

	    let y = round(rmode, result, '1);
	    out = tagged Valid tpl_1(y);
	    exc = exc | tpl_2(y);
	 end
      end

      if (out matches tagged Invalid) begin
	 // $display("result = ", fshow(result));
	 // $display("rsfd = 'h%x", rsfd);
	 // $display("zeros = %d", countZerosMSB(rsfd));

	 match {.out_, .guard_, .exc_} = normalize(result, rsfd);
	 result = out_;
	 guard = guard_;
	 exc = exc | exc_;

	 // $display("result = ", fshow(result));
	 // $display("guard = 'b%b", guard);
	 // $display("exc = 'b%b", pack(exc));
      end

      return tuple5(out,exc,rmode,result,guard);
   endfunction

   function Tuple2#(FloatingPoint#(e,m),
		Exception) s5_stage(Tuple5#(Maybe#(FloatingPoint#(e,m)),
					    Exception,
					    RoundMode,
					    FloatingPoint#(e,m),
					    Bit#(2)) op);

      match {.out,.exc,.rmode,.result,.guard} = op;

      if (out matches tagged Valid .x)
	 result = x;
      else begin
	 match {.out_, .exc_} = round(rmode,result,guard);
	 result = out_;
	 exc = exc | exc_;
      end

      return tuple2(result,exc);
   endfunction

   return s5_stage( s4_stage( s3_stage( s1_stage(tuple3(in1,in2,rmode)) ) ) );
endfunction

////////////////////////////////////////////////////////////////////////////////
/// Square root
////////////////////////////////////////////////////////////////////////////////
function Tuple2#(FloatingPoint#(e,m),Exception) sqrtFP (FloatingPoint#(e,m) in1, RoundMode rmode)
   provisos(
      Div#(m,2,mh),
      Add#(mh,2,mh1),
      Mul#(mh1,2,nsfd),
      // per request of bsc
      Add#(1, a__, m),
      Add#(b__, TLog#(TAdd#(1, TAdd#(3, m))), TAdd#(e, 1)),
      Add#(c__, 2, TMul#(nsfd, 2)),
      Add#(1, d__, TMul#(nsfd, 2)),
      Add#(m, e__, TMul#(nsfd, 2)),
      Add#(f__, TLog#(TAdd#(1, TMul#(nsfd, 2))), TAdd#(e, 1))
      );
   FloatingPoint#(e,m) out = defaultValue;
   Exception exc = defaultValue;

   if (isSNaN(in1)) begin
      out = nanQuiet(in1);
      exc.invalid_op = True;
   end
   else if (isQNaN(in1) || isZero(in1) || (isInfinity(in1) && (in1.sign == False))) begin
      out = in1;
   end
   else if (in1.sign) begin
      out = qnan();
      exc.invalid_op = True;
   end
   else begin
      out = in1;
      Int#(e) exp = unpack(unbias(out));
      out.exp = pack(exp >> 1) + fromInteger(bias(out));

      Bit#(TMul#(nsfd,2)) sfd = zExtendLSB({1'b0,getHiddenBit(in1),in1.sfd});
      Bit#(TMul#(nsfd,2)) res = 0;
      Bit#(TMul#(nsfd,2)) bits = reverseBits(extend(2'b10));

      if ((in1.exp & 1) == 0) begin
	 out.exp = out.exp + 1;
	 sfd = sfd >> 1;
      end

      // bits is highest power of 4 less than or equal to sfd
      let s0 = countZerosMSB(sfd);
      let b0 = countZerosMSB(bits);
      if (s0 > 0) begin
	 let shift = (s0 - b0) & ~'b1;
	 bits = bits >> shift;
      end

      while (bits != 0) begin
	 let sum = res + bits;

	 if (sfd >= sum) begin
	    sfd = sfd - sum;
	    res = (res >> 1) + bits;
	 end
	 else begin
	    res = res >> 1;
	 end

	 bits = bits >> 2;
      end

      sfd = sfd << (valueOf(nsfd) - 1);

      // normalize the result
      let y = normalize(out, sfd);
      out = tpl_1(y);
      exc = tpl_3(y);

      let x = round(rmode, out, tpl_2(y));
      out = tpl_1(x);
      exc = exc | tpl_2(x);
   end

   return tuple2(out,exc);
endfunction*/


////////////////////////////////////////////////////////////////////////////////
/// Pipelined Floating Point Adder
////////////////////////////////////////////////////////////////////////////////
typedef struct {
   Maybe#(FloatingPoint#(e,m,b)) res;
   RoundMode rmode;
   } CommonState#(numeric type e, numeric type m, numeric type b) deriving (Bits, Eq);

module mkFloatingPointAdder(Server#(Tuple3#(FloatingPoint#(e,m,b), FloatingPoint#(e,m,b), RoundMode), FloatingPoint#(e,m,b)))
   provisos(
      // per request of bsc
      Add#(a__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(e, 1)),
      Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(b, 1)),
      Add#(c__, e, TAdd#(b, 1)),
      Add#(d__, e, TAdd#(b, 2))
      );

   ////////////////////////////////////////////////////////////////////////////////
   /// S0
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple3#(FloatingPoint#(e,m,b),
		 FloatingPoint#(e,m,b),
		 RoundMode))                 fOperands_S0        <- mkLFIFO;

   ////////////////////////////////////////////////////////////////////////////////
   /// S1 - subtract exponents
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple8#(CommonState#(e,m,b),
		 Bit#(TAdd#(m,5)),
		 Bit#(TAdd#(m,5)),
		 Bool,
		 Bool,
       Bit#(e),
		 Bit#(b),
		 Bit#(b))) fState_S1 <- mkLFIFO;

   rule s1_stage;
      match { .opA, .opB, .rmode } <- toGet(fOperands_S0).get;

      CommonState#(e,m,b) s = CommonState {
	 res: tagged Invalid,
	 rmode: rmode
	 };

      Int#(TAdd#(b,2)) expA = zeroExtend(unpack(opA.exp)) - zeroExtend(unpack(opA.bias));
      Int#(TAdd#(b,2)) expB = zeroExtend(unpack(opB.exp)) - zeroExtend(unpack(opB.bias));

      Bit#(TAdd#(m,5)) sfdA = {1'b0, getHiddenBit(opA), opA.sfd, 3'b0};
      Bit#(TAdd#(m,5)) sfdB = {1'b0, getHiddenBit(opB), opB.sfd, 3'b0};

      Bit#(TAdd#(m,5)) x;
      Bit#(TAdd#(m,5)) y;
      Bool sgn;
      Bool sub;
      Bit#(e) exp;
      Bit#(b) bias;
      Bit#(b) expdiff;

      if ((expB > expA) || ((expB == expA) && (sfdB > sfdA))) begin
	 exp = opB.exp;
	 expdiff = truncate(pack(expB - expA));
    bias = opB.bias;
	 x = sfdB;
	 y = sfdA;
	 sgn = opB.sign;
	 sub = (opB.sign != opA.sign);
      end
      else begin
	 exp = opA.exp;
	 expdiff = truncate(pack(expA - expB));
    bias = opA.bias;
	 x = sfdA;
	 y = sfdB;
	 sgn = opA.sign;
	 sub = (opA.sign != opB.sign);
      end

      fState_S1.enq(tuple8(s,
			   x,
			   y,
			   sgn,
			   sub,
			   exp,
            bias,
			   expdiff));
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S2 - align significands
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple7#(CommonState#(e,m,b),
		 Bit#(TAdd#(m,5)),
		 Bit#(TAdd#(m,5)),
		 Bool,
		 Bool,
		 Bit#(e),
       Bit#(b))) fState_S2 <- mkLFIFO;

   rule s2_stage;
      match {.s, .opA, .opB, .sign, .subtract, .exp, .bias, .diff} <- toGet(fState_S1).get;

      if (s.res matches tagged Invalid) begin
	 if (diff < fromInteger(valueOf(m) + 5)) begin
	    Bit#(TAdd#(m,5)) guard = opB;

	    guard = opB << (fromInteger(valueOf(m) + 5) - diff);
	    opB = opB >> diff;
	    opB[0] = opB[0] | (|guard);
	 end
	 else if (|opB == 1) begin
	    opB = 1;
	 end
      end

      fState_S2.enq(tuple7(s,
			   opA,
			   opB,
			   sign,
			   subtract,
			   exp,
            bias));
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S3 - add/subtract significands
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple7#(CommonState#(e,m,b),
		 Bit#(TAdd#(m,5)),
		 Bit#(TAdd#(m,5)),
		 Bool,
		 Bool,
		 Bit#(e),
       Bit#(b))) fState_S3 <- mkLFIFO;

   rule s3_stage;
      match {.s, .a, .b, .sign, .subtract, .exp, .bias} <- toGet(fState_S2).get;

      let sum = a + b;
      let diff = a - b;

      fState_S3.enq(tuple7(s,
			   sum,
			   diff,
			   sign,
			   subtract,
			   exp,
            bias));
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S4 - normalize
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(FloatingPoint#(e,m,b)) fState_S4 <- mkLFIFO;

   rule s4_stage;
      match {.s, .addres, .subres, .sign, .subtract, .exp, .bias} <- toGet(fState_S3).get;

      FloatingPoint#(e,m,b) out = defaultValue;
      Bit#(2) guard = 0;

      if (s.res matches tagged Invalid) begin
	 Bit#(TAdd#(m,5)) result;

	 if (subtract) begin
	    result = subres;
	 end
	 else begin
            result = addres;
	 end

	 out.sign = sign;
	 out.exp = exp;
    out.bias = bias;

	//  $display("add out = ", fshow(out));
	//  $display("add result = 'h%x", result);
	//  $display("add zeros = %d", countZerosMSB(result));

	 let y = normalize(out, result);
	 out = tpl_1(y);
	 guard = tpl_2(y);
      end

      fState_S4.enq(out);
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// Interface Connections / Methods
   ////////////////////////////////////////////////////////////////////////////////
   interface request = toPut(fOperands_S0);
   interface response = toGet(fState_S4);

endmodule: mkFloatingPointAdder

module mkFloatingPointMultiplier(Server#(Tuple3#(FloatingPoint#(e,m,b), FloatingPoint#(e,m,b), RoundMode), FloatingPoint#(e,m,b)))
   provisos(
      // per request of bsc
      Add#(a__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(e, 1)),
      Add#(e__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(b, 1)),
      Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(b, 1)),
      Add#(c__, e, TAdd#(b, 1)),
      Add#(d__, e, TAdd#(b, 2))
      );

   ////////////////////////////////////////////////////////////////////////////////
   /// S0
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple3#(FloatingPoint#(e,m,b),
		 FloatingPoint#(e,m,b),
		 RoundMode))                 fOperands_S0        <- mkLFIFO;

   ////////////////////////////////////////////////////////////////////////////////
   /// S1 - calculate the new exponent/sign
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple5#(CommonState#(e,m,b),
		 Bit#(TAdd#(m,1)),
		 Bit#(TAdd#(m,1)),
		 Int#(TAdd#(b,2)),
		 Bool)) fState_S1 <- mkLFIFO;

   rule s1_stage;
      match { .opA, .opB, .rmode } <- toGet(fOperands_S0).get;

      CommonState#(e,m,b) s = CommonState {
	 res: tagged Invalid,
	 rmode: rmode
	 };

    Int#(TAdd#(b,2)) expA = zeroExtend(unpack(opA.exp)) - zeroExtend(unpack(opA.bias));
    Int#(TAdd#(b,2)) expB = zeroExtend(unpack(opB.exp)) - zeroExtend(unpack(opB.bias));
      Int#(TAdd#(b,2)) newexp = expA + expB;

      Bool sign = (opA.sign != opB.sign);

      Bit#(TAdd#(m,1)) opAsfd = { getHiddenBit(opA), opA.sfd };
      Bit#(TAdd#(m,1)) opBsfd = { getHiddenBit(opB), opB.sfd };

   if (isZero(opA) || isZero(opB)) begin
	 s.res = tagged Valid zero(opA.sign != opB.sign);
   end
   else if (newexp > fromInteger(maxexp(opA))) begin
	 FloatingPoint#(e,m,b) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = '1;
	 out.sfd = '1;
    out.bias = 0;

	//  s.exc.overflow = True;
	//  s.exc.inexact = True;

	//  let y = round(rmode, out, '1);
	 s.res = tagged Valid out;
      end
   else if (newexp < (fromInteger(minexp_subnormal(opA))-2)) begin
	 FloatingPoint#(e,m,b) out;
	 out.sign = (opA.sign != opB.sign);
	 out.exp = 0;
	 out.sfd = 0;
    out.bias = 0;

	//  s.exc.underflow = True;
	//  s.exc.inexact = True;

	//  let y = round(rmode, out, 'b01);
	 s.res = tagged Valid out;
	//  s.exc = s.exc | tpl_2(y);
      end

      fState_S1.enq(tuple5(s,
			   opAsfd,
			   opBsfd,
			   newexp,
			   sign));
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S2
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple4#(CommonState#(e,m,b),
		 Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		 Int#(TAdd#(b,2)),
		 Bool)) fState_S2 <- mkLFIFO;

   rule s2_stage;
      match {.s, .opAsfd, .opBsfd, .exp, .sign} <- toGet(fState_S1).get;

      Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))) sfdres = primMul(opAsfd, opBsfd);

      fState_S2.enq(tuple4(s,
			   sfdres,
			   exp,
			   sign));
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S3
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(Tuple4#(CommonState#(e,m,b),
		 Bit#(TAdd#(TAdd#(m,1),TAdd#(m,1))),
		 Int#(TAdd#(b,2)),
		 Bool)) fState_S3 <- mkLFIFO;

   rule s3_stage;
      let x <- toGet(fState_S2).get;
      fState_S3.enq(x);
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S4
   ////////////////////////////////////////////////////////////////////////////////
   FIFO#(FloatingPoint#(e,m,b)) fState_S4 <- mkLFIFO;

   rule s4_stage;
      match {.s, .sfdres, .exp, .sign} <- toGet(fState_S3).get;

      FloatingPoint#(e,m,b) result = defaultValue;
      Bit#(2) guard = ?;

      if (s.res matches tagged Invalid) begin
	 //$display("sfdres = 'h%x", sfdres);

	 let shift = fromInteger(minexp(result)) - exp;
    if (shift > 1) begin
	   // subnormal
	   Bit#(1) sfdlsb = |(sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift));

	   //$display("sfdlsb = |'h%x = 'b%b", (sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift)), sfdlsb);

      sfdres = sfdres >> (shift-1);
      sfdres[0] = sfdres[0] | sfdlsb;

	   result.exp = 0;
      result.bias = '1;
	 end
	 else if (shift == 1) begin
	   // subnormal
	   Bit#(1) sfdlsb = |(sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift));

	   //$display("sfdlsb = |'h%x = 'b%b", (sfdres << (fromInteger(valueOf(TAdd#(TAdd#(m,1),TAdd#(m,1)))) - shift)), sfdlsb);

      sfdres = sfdres >> 1;
      sfdres[0] = sfdres[0] | sfdlsb;

	   result.exp = 0;
      result.bias = '1-1;
	 end
	 else if (exp > 0) begin
      result.exp = truncate(pack(exp));
      result.bias = 0;
    end
    else begin
      result.exp = 1;
	   result.bias = cExtend(-exp + 1);
	 end
    result.sign = sign;

	//  $display("mul shift = %d", shift);
	//  $display("mul sfdres = 'h%x", sfdres);
	//  $display("mul result = ", fshow(result));
	//  $display("mul zeros = %d", countZerosMSB(sfdres));
	 
	 let y = normalize(result, sfdres);
	 result = tpl_1(y);
	 guard = tpl_2(y);
	//  s.exc = s.exc | tpl_3(y);

	//  $display("mul result = ", fshow(result));
	 // $display("exc = 'b%b", pack(exc));
      end

      fState_S4.enq(result);
   endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// S5
   ////////////////////////////////////////////////////////////////////////////////
   // FIFO#(Tuple2#(FloatingPoint#(e,m,b),Exception)) fResult_S5          <- mkLFIFO;

   // rule s5_stage;
   //    match {.s, .rnd, .guard} <- toGet(fState_S4).get;

   //    FloatingPoint#(e,m,b) out = rnd;

   //    if (s.res matches tagged Valid .x)
	//  out = x;
   //    else begin
	//  let y = round(s.rmode, out, guard);
	//  out = tpl_1(y);
	//  s.exc = s.exc | tpl_2(y);
   //    end

   //    fResult_S5.enq(tuple2(out,s.exc));
   // endrule

   ////////////////////////////////////////////////////////////////////////////////
   /// Interface Connections / Methods
   ////////////////////////////////////////////////////////////////////////////////
   interface request = toPut(fOperands_S0);
   interface response = toGet(fState_S4);

endmodule: mkFloatingPointMultiplier

////////////////////////////////////////////////////////////////////////////////
/// Floating point divider
////////////////////////////////////////////////////////////////////////////////
module mkFloatingPointDivider#(Server#(Tuple2#(UInt#(nbits),UInt#(dbits)),Tuple2#(UInt#(dbits),UInt#(dbits))) div)(Server#(Tuple3#(FloatingPoint#(e,m,b), FloatingPoint#(e,m,b), RoundMode), FloatingPoint#(e,m,b)))
   provisos(
      Add#(e,1,ebits),
      Add#(ebits,1,ebits1),
      Add#(m,1,m1bits),
      Add#(m,5,dbits),
      Add#(dbits,1,dbits1),
      Add#(dbits,dbits,nbits),
      // per bsc request
      Mul#(2, dbits, nbits),
      Add#(1, nbits, TAdd#(dbits, a__)),
      Add#(m, b__, dbits1),
      Add#(c__, TLog#(TAdd#(1, dbits1)), TAdd#(e, 1)),
      Add#(d__, e, ebits1),
      Add#(e__, TLog#(TAdd#(1, m1bits)), ebits1),
      Add#(2, f__, dbits),
      Add#(2, g__, dbits1),
      Add#(h__, e, TAdd#(b, 1)),
      Add#(i__, TLog#(TAdd#(1, dbits1)), TAdd#(b, 1)),
      Add#(j__, TLog#(TAdd#(1, m1bits)), TAdd#(b, 2)),
      Add#(k__, e, TAdd#(b, 2))
      );
   FIFO#(Tuple3#(FloatingPoint#(e,m,b),
		 FloatingPoint#(e,m,b),
		 RoundMode))                     fOperands_S0        <- mkLFIFO;

   //Server#(Tuple2#(UInt#(nbits),UInt#(dbits)),Tuple2#(UInt#(dbits),UInt#(dbits))) div <- mkDivider(1);

   FIFO#(Tuple5#(Maybe#(FloatingPoint#(e,m,b)),
		 FloatingPoint#(e,m,b),
		 Bit#(nbits),
		 Bit#(dbits),
		 Bit#(b))) fState_S1 <- mkLFIFO;

   rule s1_stage;
      match {.in1, .in2, .rmode} <- toGet(fOperands_S0).get;
      Maybe#(FloatingPoint#(e,m,b)) out = tagged Invalid;
      FloatingPoint#(e,m,b) result = defaultValue;
      Bit#(m1bits) sfdA = {getHiddenBit(in1), in1.sfd};
      Bit#(m1bits) sfdB = {getHiddenBit(in2), in2.sfd};
      Bit#(b) shift = 0;

      let zerosA = countZerosMSB(sfdA);
      sfdA = sfdA << zerosA;

      let zerosB = countZerosMSB(sfdB);
      sfdB = sfdB << zerosB;

      // calculate the new exponent
      Int#(TAdd#(b,2)) expA = zeroExtend(unpack(in1.exp)) - zeroExtend(unpack(in1.bias));
    Int#(TAdd#(b,2)) expB = zeroExtend(unpack(in2.exp)) - zeroExtend(unpack(in2.bias));
    Int#(TAdd#(b,2)) newexp = (expA - zeroExtend(unpack(pack(zerosA)))) - (expB - zeroExtend(unpack(pack(zerosB))));

      Bit#(nbits) opA = zExtendLSB({ 1'b0, sfdA });
      Bit#(dbits) opB = zExtend({ sfdB, 4'b0000 });

      // $display("opA: %x, zerosA : %d, opB: %x, zerosB: %d",opA,zerosA,opB,zerosB);

      // calculate the sign
      result.sign = in1.sign != in2.sign;

    if (newexp > fromInteger(maxexp(in1)+1)) begin
	 result.exp = '1;
	 result.sfd = '1;
    result.bias = 0;
	 out = tagged Valid result;
      end
      else if (newexp < (fromInteger(minexp_subnormal(in1))-2)) begin
	 result.exp = 0;
	 result.sfd = 0;
    result.bias = 0;
	 out = tagged Valid result;
      end
      else if (newexp < fromInteger(minexp(result))) begin
	 result.exp = 0;
    result.bias = '1-1;
	 shift = cExtend(fromInteger(minexp(result)) - newexp);
      end
      else if (newexp > 0) begin
	 result.exp = cExtend(newexp);
    result.bias = 0;
      end
      else begin
         result.exp = 1;
         result.bias = cExtend(-newexp + 1);
      end

      fState_S1.enq(tuple5(out,result,opA,opB,shift));
   endrule

   FIFO#(Tuple3#(Maybe#(FloatingPoint#(e,m,b)),
		 FloatingPoint#(e,m,b),
		 Bit#(b))) fState_S2 <- mkSizedFIFO(16);

   rule s2_stage;
      match {.out,.result,.opA,.opB,.shift} <- toGet(fState_S1).get;

      if (out matches tagged Invalid) begin
	//  $display("%d / %d", opA, opB);
	 div.request.put(tuple2(unpack(opA),unpack(opB)));
      end

      fState_S2.enq(tuple3(out,result,shift));
   endrule

   FIFO#(Tuple3#(Maybe#(FloatingPoint#(e,m,b)),
		 FloatingPoint#(e,m,b),
		 Bit#(dbits1))) fState_S3 <- mkLFIFO;

   rule s3_stage_div (fState_S2.first matches {tagged Invalid,.result,.shift});
      fState_S2.deq;

      Bit#(dbits1) rsfd;

	 match {.q,.p} <- div.response.get;

	 if (shift < fromInteger(valueOf(dbits1))) begin
            UInt#(dbits1) qdbits1 = extend(q);
	    Bit#(1) sfdlsb = |(pack(qdbits1 << (fromInteger(valueOf(dbits1)) - shift)));
	    rsfd = cExtend(q >> shift);
	    rsfd[0] = rsfd[0] | sfdlsb;
	 end
	 else begin
	    Bit#(1) sfdlsb = |(pack(q));
	    rsfd = 0;
	    rsfd[0] = sfdlsb;
	 end

	 if (p != 0) begin
	    rsfd[0] = 1;
	 end

	//  $display(" = %d, %d", q, p);

      fState_S3.enq(tuple3(tagged Invalid,result,rsfd));
   endrule

   rule s3_stage_no_div (fState_S2.first matches {tagged Valid .res,.result,.shift});
      fState_S2.deq;
      fState_S3.enq(tuple3(tagged Valid res,result,?));
   endrule

   FIFO#(FloatingPoint#(e,m,b)) fState_S4 <- mkLFIFO;

   rule s4_stage;
      match {.out,.result,.rsfd} <- toGet(fState_S3).get;

      Bit#(2) guard = ?;

      if (result.exp == '1) begin
	      if (truncateLSB(rsfd) == 2'b00) begin
	         rsfd = rsfd << 1;
	         result.exp = result.exp - 1;
	      end
         else begin
            result.exp = '1;
            result.sfd = '1;
            result.bias = 0;
            out = tagged Valid result;
         end
      end

      if (out matches tagged Invalid) begin
	//  $display("result = ", fshow(result));
	//  $display("rsfd = 'h%x", rsfd);
	//  $display("zeros = %d", countZerosMSB(rsfd));

	 match {.out_, .guard_} = normalize(result, rsfd);
	 result = out_;

	//  $display("result = ", fshow(result));
	//  $display("guard = 'b%b", guard);
	 // $display("exc = 'b%b", pack(exc));
      end

      fState_S4.enq(result);
   endrule

   interface request = toPut(fOperands_S0);
   interface response = toGet(fState_S4);
endmodule

(* synthesize *)
module mkFloatingPointDividerCfloat143(Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3));
   Server#(Tuple2#(UInt#(16),UInt#(8)),Tuple2#(UInt#(8),UInt#(8))) _div <- mkDivider(1);
   Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) _ifc <- mkFloatingPointDivider(_div);
   return _ifc;
endmodule

endpackage: Floatingpoint
