package tb_float;

import Floatingpoint::*;
import ActivationFunc::*;
import ClientServer::*;
import GetPut::*;

(*synthesize*)
module mktb_float(Empty);
    //Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpadder <- mkFloatingPointAdder();
    //Server#(Tuple3#(Cfloat_1_4_3, Cfloat_1_4_3, RoundMode), Cfloat_1_4_3) fpmul <- mkFloatingPointMultiplier();
    //let fpdiv <- mkFloatingPointDividerCfloat143();
    
    Ifc_FloatingPointExp  exp <- mkFloatingPointExp(); 

    Reg#(Bit#(4)) done <- mkReg(0);
    Reg#(Bit#(1)) done_add <- mkReg(0);
    Reg#(Bit#(1)) done_mul <- mkReg(0);
    Reg#(Bit#(10)) counter <- mkReg(0);
    
     Reg#(Bit#(2)) rg_state <- mkReg(0);

    rule inc_counter;
        counter <= counter + 1;
    endrule

    rule send_input(/*done!=4'd8*/rg_state ==0);
    Cfloat_1_4_3 v1;
    v1.sign = True;
    v1.exp = 4'b0001;
    v1.sfd = 3'b111;
    v1.bias= 6'd8;
    /*v1.sign = False;
    v1.exp = 4'b0000;
    v1.sfd = 3'b001;
    v1.bias= 6'd0;*/
    /*Cfloat_1_4_3 v2;
    v2.sign = False;
    v2.exp = 4'b1 + done;
    v2.sfd = 3'b01;
    v2.bias= 6'd20;*/
   // fpadder.request.put(tuple3(v1,v2,Rnd_Nearest_Even));
   // fpmul.request.put(tuple3(v1,v2,Rnd_Nearest_Even));
   // fpdiv.request.put(tuple3(v1,v2,Rnd_Nearest_Even));
   
    let lv_res <-  exp.ma_start(v1, Rnd_Nearest_Even, 3'b010); 
    $display("%d: Sent input ",counter,fshow(v1));
    $display("%b: Sent input ",v1);
     $display("%b fixval",lv_res);   
    done <= done + 1;
    rg_state <= 1; 
    endrule

   /* rule read_output_add;
        let v <- fpadder.response.get();
        $display("%d: Adder response: ",counter,fshow(v));
        // done_add <= 1;
    endrule

    rule read_output_mul;
        let v <- fpmul.response.get();
        $display("%d: Mul response: ",counter,fshow(v));
        // done_mul <= 1;
    endrule

    rule read_output_div;
        let v <- fpdiv.response.get();
        $display("%d: Div response: ",counter,fshow(v));
        // done_mul <= 1;
    endrule*/
    
     //rule read_output_add(/*done!=4'd8*/rg_state == 1 );
      //  let v <-exp.mav_out();
      //  $display("%d: exponent response: ",counter,fshow(v));
      //  $display("%b: exponent response:  ",v);
        // done_add <= 1;
      //  rg_state <= 2; 
        
 //   endrule
    
    rule endsim(rg_state == 1);
        $finish();
    endrule

    /*rule endsim((done_add==1) && (done_mul==1));
        $finish();
    endrule*/
endmodule

endpackage:tb_float
