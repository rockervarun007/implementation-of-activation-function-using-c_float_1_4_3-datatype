// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmktb_float.h for the primary calling header

#include "verilated.h"

#include "Vmktb_float__Syms.h"
#include "Vmktb_float___024root.h"

#ifdef VL_DEBUG
VL_ATTR_COLD void Vmktb_float___024root___dump_triggers__stl(Vmktb_float___024root* vlSelf);
#endif  // VL_DEBUG

VL_ATTR_COLD void Vmktb_float___024root___eval_triggers__stl(Vmktb_float___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vmktb_float__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmktb_float___024root___eval_triggers__stl\n"); );
    // Body
    vlSelf->__VstlTriggered.at(0U) = (0U == vlSelf->__VstlIterCount);
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vmktb_float___024root___dump_triggers__stl(vlSelf);
    }
#endif
}
