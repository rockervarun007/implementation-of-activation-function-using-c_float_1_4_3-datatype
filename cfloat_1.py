import math

def get_bin(h):
    temp = bin(h)[2:]
    return "0"*(8-len(temp))+temp
    
vals = []
for i in range(256):
    vals.append(get_bin(i))
    
# cfloat8_1_4_3
class Cfloat143:
    def __init__(self,sign,exp,sfd) -> None:
        self.sign = sign
        self.exp = exp
        self.sfd = sfd

    def get_val(self):
        hidd = 1 if self.exp!=-63 else 0
        return (hidd + (self.sfd/8))*self.sign * (2**(self.exp))
    
    def gen_cfloat143(self):
        if self.exp > 0:
            exp = self.exp
            bias = 0
        else:
            exp = 1
            bias = (1 - self.exp)

flt_vals = []
for sign in [-1,1]:
    for exp in range(-63,16):
        for sfd in range(8):
            flt_vals.append(Cfloat143(sign,exp,sfd))

flt = []
for b in flt_vals:
    flt.append(b.get_val())

flt = sorted(flt)

def binary_search(arr, x):
    low = 0
    high = len(arr) - 1
    mid = 0
 
    while low <= high:
 
        mid = (high + low) // 2
 
        # If x is greater, ignore left half
        if arr[mid] < x:
            low = mid + 1
 
        # If x is smaller, ignore right half
        elif arr[mid] > x:
            high = mid - 1
 
        # means x is present at mid
        else:
            return mid
 
    # If we reach here, then the element was not present
    return -1
 
def findClosest(arr, n, target):
    left, right = 0, n - 1
    while left < right:
        if abs(arr[left] - target) <= abs(arr[right] - target):
            right -= 1
        else:
            left += 1
    return arr[left]
 

bias = 0


def get_cfloat_val(bst,bias):
    sign = -1 if (bst[0]=='1') else 1
    exp = int(bst[1:5],2) - bias
    mantissa = int(bst[5:8],2)
    if (exp + bias) != 0:
        mantissa += 8
    mantissa /= (8)
    return str(sign)[0:-1]+str(mantissa)+" x 2^("+str(exp)+")",sign*mantissa*(2**exp)
print(vals)
for v in vals:
    for w in range(64):
        a,b = get_cfloat_val(v,w)
        if(b < 700):
         ex = math.exp(b)
        fc = findClosest(flt,len(flt),ex)
        print(v + "\t" + str(w) + "\t" + a+"\t\t"+str(b) + "\t" + str(ex) + "\t" + str(fc))
        
    
